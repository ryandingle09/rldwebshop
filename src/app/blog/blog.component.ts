import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { 
    BlogService,
    SeoService,
    BlogModel,
    CategoryModel,
    CmsModel,
    StorageService
} from '../shared';
@Component({
    selector: 'app-blog',
    templateUrl: './blog.component.html',
})
export class BlogComponent implements OnInit {

    public maxSize:number = 5;
    public bigTotalItems:number = 0;
    public bigCurrentPage = 1;
    public numPages:number = 5;
    public itemsPerPage: number = 10;
    public bigCurrenItems:number = 1;
    public post: BlogModel[] = [];
    public offset:number = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage*this.itemsPerPage;
    public datas = {
        'value': '',
        'per_page' : this.itemsPerPage,
        'offset': this.offset
    };
    public categories: CategoryModel[];
    public loading_articles = true;
    public loading_categories = true;
    public blog_data: CmsModel[];
    public image: any = '/assets/images/headers/index2.jpg';

    constructor(
        private blog: BlogService,
        private router: Router,
        private seo: SeoService,
        private storage: StorageService
    ) { }

    ngOnInit() {
        this.blog_data = this.storage.get('blog');
          this.image   = this.blog_data[0].image == null ? this.image : this.blog_data[0].image;
        /*SEO META UPDATE*/
        this.seo.updateKeyword('RLD Webshop, Blog Page, Blog');
        this.seo.updateTitle('RLD Webshop - Blog');
        this.seo.updateDescription('RLD Webshop Blog Website');
        this.seo.updateAuthor('Ryan L. Dingle');
        this.seo.updateImage(this.image);

        this.blog.list(this.datas).then(response => { 
            this.post = response[0].data; 
            this.bigTotalItems = response[0].total;
            this.loading_articles = false;
        });

        this.blog.getCategoryList().then(response=> {
            this.categories = response;
            this.loading_categories = false;
        });
    }

    public perPage(value) {
        this.itemsPerPage = value;
        this.ngOnInit();
    }
     
    public pageChanged(event:any):void {
        this.bigCurrentPage = event.page;
        this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage*this.itemsPerPage;
        this.ngOnInit();
    }

}
