import { ModuleWithProviders, NgModule } from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { BlogComponent } from './blog.component';
import { BlogDetailComponent } from './blog-detail.component';
import { BlogCategoryComponent } from './blog-category.component';
import {DisqusModule} from "ng2-awesome-disqus";
import {ShareButtonsModule} from 'ngx-sharebuttons';
import { AdsenseModule } from 'ng2-adsense';
import { 
  SharedModule,
  CmsService
} from '../shared'; 

const Routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'blog',
    component: BlogComponent,
    data: {
      page: 'blog',
      title: 'RLD Webshop - Blog'
    }
  },
  {
    path: 'blog/:slug',
    component: BlogDetailComponent,
    data: {
      page: 'blogdetail',
      title: 'RLD Webshop - Blog Details'
    }
  },
  {
    path: 'blog/cat/:id',
    component: BlogCategoryComponent,
    data: {
      page: 'blog',
      title: 'RLD Webshop - Blog Category'
    }
  }
]);

@NgModule({
  imports: [
    Routing,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    DisqusModule,
    ShareButtonsModule.forRoot(),
    NoopAnimationsModule,
    AdsenseModule.forRoot({
        adClient: 'ca-pub-2121261107511559',
        adSlot: 3278764682
    }), 
  ],
  declarations: [
    BlogComponent,
    BlogDetailComponent,
    BlogCategoryComponent
  ],
  providers: [CmsService]
})
export class BlogModule {}