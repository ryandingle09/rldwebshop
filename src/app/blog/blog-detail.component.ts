import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { 
    BlogService,
    SeoService 
} from '../shared/services';
import { 
    BlogModel 
} from '../shared/models';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html'
})

export class BlogDetailComponent implements OnInit {

  constructor(
    private blog: BlogService,
    private route: ActivatedRoute,
    private router: Router,
    private seo: SeoService) { }

  public post: BlogModel;
  public slug = this.route.snapshot.params['slug'];
  public recentPost: BlogModel[];
  public pageId = '/blog/'+this.slug;
  public loading_articles = true;
  public loading_categories = true;

  ngOnInit() {
    this.blog.getBySlug(this.slug).then(response => { 
        this.post = response[0]; 
        this.seo.setTitle('Ryan Dingle - '+this.post.title);
        this.loading_articles = false;

        /*SEO META UPDATE*/
        this.seo.updateKeyword(this.post.tags);
        this.seo.updateTitle(this.post.title);
        this.seo.updateDescription(response[0].body);
        this.seo.updateSummary(response[0].body);
        this.seo.updateImage(this.post.image);
    });
    this.blog.getRecentPost().then(response=> {
      this.recentPost = response;
      this.loading_categories = false;
    });
  }

  navigate(slug) {
    this.loading_articles = true;
    this.loading_categories = true;    
    this.route.params.subscribe(res => { 
      this.router.navigate(['blog', slug]).then(res=>{
        this.pageId = '/works/'+slug;
        this.blog.getBySlug(slug).then(response => { 
            this.post = response[0];
            this.loading_articles = false; 
            /*SEO META UPDATE*/
            this.seo.updateKeyword(this.post.tags);
            this.seo.updateTitle(this.post.title);
            this.seo.updateDescription(response[0].body);
            this.seo.updateSummary(response[0].body);
            this.seo.updateImage(this.post.image);
        });
        this.blog.getRecentPost().then(response=> {
          this.recentPost = response;
          this.loading_categories = false;
        });
      });
    });
  }

}
