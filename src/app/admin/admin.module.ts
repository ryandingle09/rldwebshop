import { ModuleWithProviders, NgModule } from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule, CanActivate } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { AdminComponent } from './admin.component';
import {  SharedModule } from '../shared';
import { CKEditorModule } from 'ng2-ckeditor';
import {
  SiteService,
  PortfolioService,
  BlogService,
  TagService,
  CategoryService,
  AuthGuard,
  AlertService,
  CounterService
} from '../shared/services';
import {NgSelectizeModule} from 'ng-selectize';
import { SimpleNotificationsModule } from 'angular2-notifications';

const Routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'admin',
      title: 'RLD Webshop - Admin Dashboard'
    }
  }
]);

@NgModule({
  imports: [
    Routing,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    NgSelectizeModule,
    NoopAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    CKEditorModule
  ],
  declarations: [
    AdminComponent,
  ],
  exports: [
    
  ],
  providers: [
    SiteService,
    PortfolioService,
    BlogService,
    TagService,
    CategoryService,
    AuthGuard,
    AlertService,
    CounterService 
  ],
  entryComponents: [
  ]
})
export class AdminModule {}