import { ModuleWithProviders, NgModule } from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { AboutComponent } from './about.component';
import { SharedModule,CmsService } from '../shared'; 

const Routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'about',
    component: AboutComponent,
    data: {
      page: 'about',
      title: 'RLD Webshop - About'
    }
  }
]);

@NgModule({
  imports: [
    Routing,
    SharedModule,
    NoopAnimationsModule
  ],
  declarations: [
    AboutComponent
  ],
  providers: [CmsService]
})
export class AboutModule {}