import { Component, OnInit } from '@angular/core';
import { 
  SeoService,
  StorageService,
  CmsModel
} from '../shared';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  public about:CmsModel[];
  public image: any = '/assets/images/headers/index2.jpg';
  public history: CmsModel[];
  public skill: CmsModel[];

  constructor(
      private seo: SeoService,
      private storage: StorageService
  ) { }

  ngOnInit() {
      this.about   = this.storage.get('about');
      this.skill   = this.storage.get('skill');
      this.history = this.storage.get('history');
      this.image   = this.about[0].image == null ? this.image : this.about[0].image;
      	/*SEO META UPDATE*/
	    this.seo.update({name: 'keywords', content: 'RLD Webshop, About'});
	    this.seo.updateTitle('RLD Webshop - About');
	    this.seo.updateDescription('RLD Webshop About Page');
	    this.seo.updateImage(this.image);
  }

}
