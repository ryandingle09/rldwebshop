import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';

import { AccountComponent } from './account.component';
import { 
  SharedModule,
  AuthGuard, 
  UserService,
  AuthService,
  CmsService 
} from '../shared'; 
import { SimpleNotificationsModule } from 'angular2-notifications';

const Routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'account',
    component: AccountComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'account',
      title: 'RLD Webshop - My Account'
    }
  }
]);

@NgModule({
  imports: [
    Routing,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    NoopAnimationsModule
  ],
  declarations: [
    AccountComponent
  ],
  providers: [
    AuthGuard, 
    AuthService,
    CmsService,
    UserService
  ]
})
export class AccountModule {}