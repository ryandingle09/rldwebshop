import { Component, OnInit, PLATFORM_ID, Inject  } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router, NavigationEnd, RoutesRecognized } from '@angular/router';
import 'rxjs/add/operator/filter';
import { isPlatformBrowser } from '@angular/common';
import { 
    CmsModel, 
    UserModel,
    SiteService, 
    SocialModel, 
    SiteModel,
    AuthService,
    SeoService,
    StorageService,
    PortfolioModel,
    CmsService,
    VisitorService
} from './shared';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit{

    public sitedata: SiteModel[];
    public socialdata: SocialModel[];
    public home: CmsModel[];
    public about: CmsModel[];
    public blog: CmsModel[];
    public portfolio: CmsModel[];
    public contact: CmsModel[];
    public skill: CmsModel[];
    public history: CmsModel[];
    public service: CmsModel[];
    public works: PortfolioModel[];
    
    public lang        = 'en_US';
    public author      = 'Ryan Dingle';
    public appId       = '259321127540805';
    public adminId     = '706724252756077';
    public appType     = 'website';
    public siteName    = 'RLD Webshop - Full Stack Web Developer';

    constructor(
        private router: Router, 
        private cms: CmsService,
        private auth: AuthService,
        private seo: SeoService,
        private storage: StorageService,
        private visit: VisitorService,
        @Inject(PLATFORM_ID) private platformId: Object
    )
    {
        router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe((event: NavigationEnd) => {
                if (isPlatformBrowser(this.platformId)) {
                    window.scroll(0, 0);
                }
        });

        this.visit.store();
    }

    ngOnInit() {

        this.seo.update({ name: 'twitter:creator', content: this.author });
        this.seo.update({ name: 'author', content: this.author });
        this.seo.update({ name: 'twitter:site', content: this.siteName });
        this.seo.update({ name: 'og:site_name', content: this.siteName });
        this.seo.update({ name: 'og:locale', content: this.lang });
        this.seo.update({ name: 'og:admins', content: this.adminId });
        this.seo.update({ name: 'og:app_id', content: this.appId });
        this.seo.update({ name: 'og:type', content: 'website' });

        this.initialize();
    }

    public initialize() {
        this.cms.getPageCms('home').then(response => {
            this.cms.broadCastCms(response);
            
            this.sitedata     = response.site;
            this.socialdata   = response.social;
            this.home         = response.home;
            this.service      = response.service;
            this.about        = response.about;
            this.blog         = response.blog;
            this.portfolio    = response.portfolio;
            this.contact      = response.contact;
            this.skill        = response.skill;
            this.history      = response.history;
            this.works        = response.project;

            this.storage.set('sitedata', response.site);
            this.storage.set('socialdata', response.social);
            this.storage.set('home', response.home);
            this.storage.set('service', response.service);
            this.storage.set('about', response.about);
            this.storage.set('blog', response.blog);
            this.storage.set('portfolio', response.portfolio);
            this.storage.set('works', response.project);
            this.storage.set('contact', response.contact);
            this.storage.set('skill', response.skill);
            this.storage.set('history', response.history);
        });
    }

}
