import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import { HomeComponent } from './home.component';
import { 
  SharedModule,
  CmsService,
  HomeService
} from '../shared'; 

const homeRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: '',
    component: HomeComponent,
    data: {
      page: 'home',
      title: 'RLD Webshop - Full Stack Web Developer'
    }
  }
]);

@NgModule({
  imports: [
    homeRouting,
    SharedModule,
    NoopAnimationsModule
  ],
  declarations: [
    HomeComponent
  ],
  providers: [CmsService, HomeService]
})
export class HomeModule {}