import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal/';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { 
  SeoService,
  CmsService,
  CmsModel,
  PortfolioModel,
  StorageService,
  Errors,
  HomeService
} from '../shared';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  public home: CmsModel[];
  public works: CmsModel[];
  public service: CmsModel[];

  public portfolio: PortfolioModel[];
  public image:any = '/assets/images/headers/index2.jpg';
  public image2:any = '/assets/images/featured2.png';
  public form: any;
  public errors: Errors;
  public modalRef: BsModalRef;
  public loading: any = false;

  constructor(
      private modalService: BsModalService,
      private seo: SeoService,
      private cms: CmsService,
      private storage: StorageService,
      private homeservice: HomeService
  ) { }

  ngOnInit() {
    this.cms.getPageCms('home').then(
      response => {
        this.works      = response.project;
        this.service    = response.service;
        this.home       = response.home;

        this.image      = (response.home[0].header_image == null || response.home[0].header_image == undefined) ? this.image : response.home[0].header_image;
        this.image2     = (response.home[0].package_image == null || response.home[0].package_image == undefined) ? this.image2 : response.home[0].package_image;
      }
    );

    this.seo.updateTitle('RLD Webshop - Home');
    this.seo.updateAuthor('Ryan L. Dingle');
    this.seo.updateSummary('Welcome to my portfolio website');
    this.seo.updateDescription('RLD Webshop is a portfolio website owned by Ryan Dingle from Makati City, Philippines');
    this.seo.updateKeyword('Ryan Dingle, RLD Webshop, ryandingle09, ryandingle');
    this.seo.updateImage(this.image);

    this.createForm();
  }

  public createForm() {
    this.form = new FormGroup({
      email: new FormControl('',[Validators.required, Validators.email])
    });
  }

  public save(template: TemplateRef<any>) {
    this.loading = true;
    let data: FormData = new FormData();
    data.append('email', this.form.value.email);

    this.homeservice.store(data).then(response => {
        if(response.message == 'success') {
          this.modalRef = this.modalService.show(template);
          this.loading = false;
          this.errors = new Errors;
        }
      },
      error => {
        this.errors = JSON.parse(error._body);
        this.loading = false;
    });
  }

}
