import { ModuleWithProviders, NgModule } from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';
import { NotificationComponent } from './notification/notification.component';
import { NotificationDetailComponent } from './notification/notification-detail.component';
import { MessageComponent } from './message/message.component';
import { ArticleComponent } from './article/article.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { SharedModule, AuthGuard, AlertService } from '../shared'; 
import { PaginationModule } from 'ngx-bootstrap';
import {NgSelectizeModule} from 'ng-selectize';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { CKEditorModule } from 'ng2-ckeditor';
import { MomentModule } from 'ngx-moment';
import {
  NotificationService, BroadCasterService
} from '../shared/services';

const Routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'notification',
    component: NotificationComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'notification',
      title: 'RLD Webshop - My Notifications'
    }
  },
  {
    path: 'notification/:type/:type_id/:id',
    component: NotificationDetailComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'notification',
      title: 'RLD Webshop - My Notifications'
    }
  },
  {
    path: 'message',
    component: MessageComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'message',
      title: 'RLD Webshop - My Messages'
    }
  },
  {
    path: 'article',
    component: ArticleComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'article',
      title: 'RLD Webshop - My Articles'
    }
  },
  {
    path: 'portfolio',
    component: PortfolioComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'portfolio',
      title: 'RLD Webshop - My Portfolio'
    }
  },
]);

@NgModule({
  imports: [
    Routing,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectizeModule,
    PaginationModule.forRoot(),
    NoopAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    CKEditorModule,
    MomentModule    
  ],
  declarations: [
    NotificationComponent,
    PortfolioComponent,
    ArticleComponent,
    MessageComponent,
    NotificationDetailComponent
  ],
  providers: [AuthGuard, NotificationService, BroadCasterService]
})
export class AppsModule {}