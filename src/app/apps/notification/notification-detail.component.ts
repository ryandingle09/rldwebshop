import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../shared/services';
import { 
	NotificationModel, 
	Errors 
} from '../../shared/models';

@Component({
  	selector: 'app-notification-detail',
  	templateUrl: './notification-detail.component.html',
})
export class NotificationDetailComponent implements OnInit {

	public errors: Errors;
	public id = this.route.snapshot.params['id'];
	public type_id = this.route.snapshot.params['type_id'];
	public type = this.route.snapshot.params['type'];
	public notification = NotificationModel;

	constructor(
		private notif: NotificationService,
		private route: ActivatedRoute,
  	private router: Router
  ) { }

	ngOnInit() {
		this.notif.get(this.type, this.type_id).then(response => {
			this.notification = response;
		}, error => {
			this.errors = JSON.parse(error._body);
		});
	}

}
