import { Component, OnInit } from '@angular/core';
import { NotificationService, BroadCasterService } from '../../shared/services';
import { 
	NotificationModel, 
	Errors 
} from '../../shared/models';

@Component({
  	selector: 'app-notification',
  	templateUrl: './notification.component.html',
})
export class NotificationComponent implements OnInit {

  	public errors: Errors;
  	public maxSize:number = 5;
  	public bigTotalItems:number = 0;
  	public bigCurrentPage = 1;
	public numPages:number = 5;
	public itemsPerPage: number = 10;
	public bigCurrenItems:number = 1;
  	public notification: NotificationModel[];
	public offset:number = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage*this.itemsPerPage;
	public datas = {
		'value': '',
		'per_page' : this.itemsPerPage,
		'offset': this.offset
	};
	public new_count:number = 0;

  	constructor(private notif: NotificationService, private broadcast: BroadCasterService) { }

  	ngOnInit() {
  		this.notif.list(this.datas).then( response => {
  			this.notification = response[0].data;
  			this.bigTotalItems = response[0].total; 
  			this.new_count = response[0].new;
  			this.broadcast.changeNumber(response[0].new);
  		}, error => {
  			this.errors = JSON.parse(error._body);
  		});
  	}

  	public read(id) {
		this.notif.read(id).then(
			response => { 
				this.ngOnInit();
				this.broadcast.changeNumber(this.new_count - 1);
			},
			error => {
				this.errors = JSON.parse(error._body)
			}
		);
	}

	public search(value) {
		this.datas.value = value;
		this.ngOnInit();
	}

	public refresh() {
		this.ngOnInit();
	}

	public readAll() {
	    this.broadcast.changeNumber(0);
	    this.notification.map((data) => {
	        if(data.id != 0) {
	            data.status = 1;
	        }
	      });
	    this.notif.readAll().then(response => {}, 
	      error => {
	      this.errors = JSON.parse(error._body);
	    });
	  }

	public perPage(value) {
		this.itemsPerPage = value;
		this.ngOnInit();
	}

	public pageChanged(event:any):void {
		this.bigCurrentPage = event.page;
		this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage*this.itemsPerPage;
		this.ngOnInit();
	}

}
