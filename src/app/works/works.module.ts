import { ModuleWithProviders, NgModule } from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { PaginationModule } from 'ngx-bootstrap';
import { WorksComponent } from './works.component';
import { WorksDetailComponent } from './works-detail.component';
import { DisqusModule } from "ng2-awesome-disqus";
import { ShareButtonsModule } from 'ngx-sharebuttons';
import { AdsenseModule } from 'ng2-adsense';
import { 
  SharedModule, 
  PortfolioService 
} from '../shared'; 

const Routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'works',
    component: WorksComponent,
    data: {
      page: 'works',
      title: 'RLD Webshop - Works/Portfolio'
    }
  },
  {
    path: 'works/:id',
    component: WorksDetailComponent,
    data: {
      page: 'works',
      title: 'RLD Webshop - Work Details'
    }
  }
]);

@NgModule({
  imports: [
    Routing,
    SharedModule,
    PaginationModule.forRoot(),
    DisqusModule,
    ShareButtonsModule.forRoot(),
    NoopAnimationsModule,
    AdsenseModule.forRoot({
        adClient: 'ca-pub-2121261107511559', 
        adSlot: 3278764682 
    }), 
  ],
  declarations: [
    WorksComponent,
    WorksDetailComponent
  ],
  providers: [
    PortfolioService
  ]
})
export class WorksModule {}