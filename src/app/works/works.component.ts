import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { 
  PortfolioService,
  SeoService,
  PortfolioModel,
  CmsModel,
  StorageService
} from '../shared';

@Component({
  selector: 'app-works',
  templateUrl: './works.component.html',
})
export class WorksComponent implements OnInit {

  public maxSize:number = 5;
  public bigTotalItems:number = 0;
  public bigCurrentPage = 1;
  public numPages:number = 5;
  public itemsPerPage: number = 12;
  public bigCurrenItems:number = 1;
  public projects: PortfolioModel[] = [];
  public offset:number = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage*this.itemsPerPage;
  public datas = {
    'value': '',
    'per_page' : this.itemsPerPage,
    'offset': this.offset
  };
  public work: CmsModel[];
  public image: any = '/assets/images/featured2.png';

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private portfolio: PortfolioService,
      private seo: SeoService,
      private storage: StorageService
  ) { }
  
  ngOnInit() {
    this.work = this.storage.get('portfolio');
    this.image   = this.work[0].image == null ? this.image : this.work[0].image;

    /*SEO META UPDATE*/
    this.seo.updateTitle('RLD Webshop - Works/Portfolio');
    this.seo.updateAuthor('Ryan L. Dingle');
    this.seo.updateSummary('RLD Webshop Portfolio, Browse portfolio');
    this.seo.updateDescription('RLD Webshop Portfolio, Browse portfolio');
    this.seo.updateKeyword('Works, Projects, Portfolio');
    this.seo.updateImage(this.image);

  	this.portfolio.list(this.datas).then(response => { 
      this.projects = response[0].data; 
      this.bigTotalItems = response[0].total;
    });

  	this.router.events.subscribe((res) => { 
      if (res instanceof RoutesRecognized) {
        let route = res.state.root.firstChild;
        let headtitle = route.data.title;
      }
    });
  }

  public perPage(value) {
    this.itemsPerPage = value;
    this.ngOnInit();
  }
 
  public pageChanged(event:any):void {
    this.bigCurrentPage = event.page;
    this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage*this.itemsPerPage;
    this.ngOnInit();
  }

}
