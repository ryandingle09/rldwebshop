import { BrowserModule, Title } from '@angular/platform-browser';
import {NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule }    from '@angular/http';
import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap';
import { AppComponent } from './app.component';
import { ShareButtonsModule } from 'ngx-sharebuttons';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AdsenseModule } from 'ng2-adsense';
import { MomentModule } from 'ngx-moment';
import { InfiniteScrollModule } from "ngx-infinite-scroll";
//import 'web-animations-js';
import {
  SharedModule,
  FooterComponent,
  HeaderComponent,
  SeoService,
  ApiService,
  StorageService,
  VisitorService,
  BroadCasterService
} from './shared';

import { HomeModule } from './home/home.module';
import { BlogModule } from './blog/blog.module';
import { AboutModule } from './about/about.module';
import { ContactModule } from './contact/contact.module';
import { WorksModule } from './works/works.module';
import { AdminModule } from './admin/admin.module';
import { AuthModule } from './auth/auth.module';
import { AccountModule } from './account/account.module';
import { SiteModule } from './site/site.module';
import { AppsModule } from './apps/apps.module';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([], { useHash: false });

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'rldwebshopv5'}),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    HomeModule,
    BlogModule,
    AboutModule,
    ContactModule,
    WorksModule,
    AdminModule,
    AuthModule,
    SiteModule,
    AccountModule,
    AppsModule,
    rootRouting,
    ModalModule.forRoot(),
    ShareButtonsModule.forRoot(),
    HttpModule,
    SimpleNotificationsModule.forRoot(),
    NoopAnimationsModule,
    BrowserAnimationsModule,
    AdsenseModule.forRoot({
        adClient: 'ca-pub-2121261107511559',
        adSlot: 3278764682
    }), 
    MomentModule,
    InfiniteScrollModule
  ],
  providers: [Title, SeoService, ApiService, StorageService, VisitorService, BroadCasterService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'on the browser' : 'in the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}

