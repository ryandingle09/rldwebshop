import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { 
  SiteModel, 
  UserModel,
  SocialModel,
  CmsModel,
  NotificationModel,
  Errors 
} from '../../shared/models';
import { 
  AuthService,
  SeoService,
  StorageService,
  NotificationService,
  BroadCasterService
} from '../../shared/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  public page: string = 'home';
  public headtitle: string;
  public location: any;
  public login: any = false;
  public status = this.auth.isLoggedIn();
  public users = UserModel;
  public image:any = '/assets/images/headers/index2.jpg';
  public new_count:number = 0;
  public errors: Errors;
  public isloading = false;
  public head_notif: NotificationModel[] = [];
  public datas = {
    'value': '',
    'per_page' : 10,
    'offset': 0
  };

  @Input() socialdata: SocialModel[];
  @Input() sitedata: SiteModel[];
  @Input() home: CmsModel[];

  constructor(
    private router: Router,
    private auth: AuthService,
    private seo: SeoService,
    private storage: StorageService,
    private notif: NotificationService,
    private broadcast: BroadCasterService
  ){ }

  ngOnInit() {
    this.router.events.subscribe((res) => { 
      this.status = this.auth.isLoggedIn();
      this.users = this.auth.getUserToken();
      if (res instanceof RoutesRecognized) {
        let route = res.state.root.firstChild;
        this.page = route.data.page;
        this.headtitle = route.data.title;
      }

      if(this.page !== 'blogdetail') this.seo.setTitle(this.headtitle);

      this.login = (this.page == 'register' || this.page == 'login' || this.page == 'home' || this.page == 'about' || this.page == 'works' || this.page == 'contact' || this.page == 'blog' || this.page == 'blogdetail') ? false : true;
    });

    if(this.status) {
      this.broadcast.currentNumber.subscribe(amount => this.new_count = amount);
      this.loadNotification();
    }
  }

  public loadNotification() {
    this.isloading = false;
    this.notif.list(this.datas).then( response => {
        for(let i = 0; i <= response[0].data.length - 1; i++) {
          this.head_notif.push(response[0].data[i]);
        }
        this.new_count = this.new_count == 0 ? response[0].new : this.new_count; 
        this.broadcast.changeNumber(this.new_count);

        this.isloading = true;
      }, error => {
        this.errors = JSON.parse(error._body);
      });
  }

  public clickAsRead(type, type_id, id) {
    this.changeStatus(id);
    this.read(id);
    this.router.navigateByUrl('/notification/'+type+'/'+type_id+'/'+id);
  }

  public read(id) {
    this.notif.read(id).then(
      response => {},
      error => {
        this.errors = JSON.parse(error._body)
      }
    );
  }

  public changeStatus(id){
      this.head_notif.map((data) => {
        if(data.id == id) {
          if(data.status == 0) {
            this.new_count = this.new_count - 1;
            this.broadcast.changeNumber(this.new_count);
            data.status = 1;
          }
        }
      });
  }

  public readAll() {
    this.broadcast.changeNumber(0);
    this.notif.readAll().then(response => {}, 
      error => {
      this.errors = JSON.parse(error._body);
    });
  }

  public onScroll() {
    this.datas.offset += 10;
    this.loadNotification();
  }

  public logout() {
    this.auth.setLogout();
    this.router.navigateByUrl('/auth/login');
  }

  pageName(page: string) { this.page = page;}
  
}