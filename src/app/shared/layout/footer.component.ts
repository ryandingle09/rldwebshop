import { Component, OnInit, Input } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { SiteModel, SocialModel, UserModel } from '../../shared/models';
import { AuthService, StorageService } from '../../shared/services';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent implements OnInit{

  public location: any;
  public socials: any[];
  public login: any = false;

  public page: string = 'home';
  public headtitle: string;
  public status = this.auth.isLoggedIn();

  public users = UserModel;
  public today: number = Date.now();

  @Input() socialdata: SocialModel[];
  @Input() sitedata: SiteModel[];

  constructor(
    private router: Router,
    private auth: AuthService,
    private storage: StorageService
  ){ }

  ngOnInit() {
    this.router.events.subscribe((res) => { 
      this.status = this.auth.isLoggedIn();
      this.users = this.auth.getUserToken();
      if (res instanceof RoutesRecognized) {
        let route = res.state.root.firstChild;
        this.page = route.data.page;
      }
      this.login = (this.page == 'register' || this.page == 'login' || this.page == 'home' || this.page == 'about' || this.page == 'works' || this.page == 'contact' || this.page == 'blog' || this.page == 'blogdetail') ? false : true;
    });
  }

  
}