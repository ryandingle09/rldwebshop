import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class CategoryService {

    constructor(private api: ApiService) {}

    public list() {
        return this.api.list('/category');
    }

    public list2(data: any) {
        return this.api.listByPost('/category/list', data);
    }

    public store(data: any) {
        return this.api.post('/category/post', data);
    }

    public edit(id:any) {
        return this.api.get('/category/'+id+'/edit');
    }

    public update(data: any, id: any) {
        return this.api.update('/category/'+id+'/update', data);
    }

    public delete(id:any) {
        return this.api.delete('/category/'+id+'/delete');
    }
}