import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class ContactService {

    constructor(private api: ApiService) {}

    public store(data) {
      return this.api.post('/message/post', data);
    }
}