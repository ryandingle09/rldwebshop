import { Injectable, EventEmitter } from '@angular/core';
import { ApiService  }  from './api.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class CmsService {

    cmsData = new EventEmitter();

    constructor(private api: ApiService) {}

    public listByPost(data, page) {
      return this.api.listByPost('/cms/'+page+'/list', data);
    }

    public broadCastCms(data: any) {
      this.cmsData.emit(data);
    }

    public getPageCms(page) {
      return this.api.get('/cms/'+page+'/pagecms');
    }

    public get(id) {
      return this.api.get('/cms/'+id+'/get');
    }

    public edit(id) {
      return this.api.get('/cms/'+id+'/edit');
    }

    public store(data) {
      return this.api.post('/cms/post', data);
    }

    public update(data, id) {
      return this.api.post('/cms/'+id+'/update', data);
    }

    public delete(id) {
      return this.api.delete('/cms/'+id+'/delete');
    }
}