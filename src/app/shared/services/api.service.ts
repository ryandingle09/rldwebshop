import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Errors } from '../models';
import '../rxjs-operator';

@Injectable()
export class ApiService {

	private url  	= 'https://api.rdingle.com/api/v1';
    //private url 	= 'http://portfolioapi.test/api/v1';
    private headers = new Headers({'Accept': 'application/json'});

    constructor(private http: Http) { }

    public listByPost(params: any = null, data: any) {
    	return this.http.post(this.url+params, data, {headers: this.headers})
        .toPromise()
        .then(response => response.json() as any[])
        .catch(this.handleError);
    }

    public list(params: any = null) {
    	return this.http.get(this.url+params, {headers: this.headers})
        .toPromise()
        .then(response => response.json() as any[])
        .catch(this.handleError);
    }

    public get(params: any = null) {
        return this.http.get(this.url+params, {headers: this.headers})
        .toPromise()
        .then(response => response.json() as any[])
        .catch(this.handleError);
    }

    public post(params: any = null, data: any) {
        return this.http.post(this.url+params, data, {headers: this.headers})
        .toPromise()
        .then(response => response.json() as Errors)
        .catch(this.handleError);
    }

    public update(params: any = null, data: any) {
        return this.http.post(this.url+params, data, {headers: this.headers})
        .toPromise()
        .then(response => response.json() as Errors)
        .catch(this.handleError);
    }

    public delete(params: any = null) {
        return this.http.delete(this.url+params, {headers: this.headers})
        .toPromise()
        .then(response => response.json() as Errors)
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}