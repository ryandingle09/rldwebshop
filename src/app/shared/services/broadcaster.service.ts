import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class BroadCasterService {

  private stringSource = new BehaviorSubject('');
  currentString = this.stringSource.asObservable();

  private numberSource = new BehaviorSubject(0);
  currentNumber = this.numberSource.asObservable();

  constructor() { }

  public changeString(text: string) {
    this.stringSource.next(text)
  }

  public changeNumber(amount: number) {
  	this.numberSource.next(amount)
  }

}