import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class CounterService {

    constructor(private api: ApiService) {}

    public get() {
      return this.api.get('/counters');
    }
}