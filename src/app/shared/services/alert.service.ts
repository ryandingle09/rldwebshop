import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { isPlatformBrowser } from '@angular/common';

@Injectable()
export class AlertService {
	constructor(
	private notify: NotificationsService,
	@Inject(PLATFORM_ID) private platformId: Object
	) {}

	public alert(message, erro_type) {
		if (isPlatformBrowser(this.platformId)) {
			this.notify.create(erro_type, message, erro_type, {
				position: ["top", "right"],
				timeOut: 3000,
			    showProgressBar: false,
		        pauseOnHover: true,
		        clickToClose: false,
			});
		}
  	}
}