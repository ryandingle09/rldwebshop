import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable()
export class SeoService {

	public lang   	= 'en_US';
	public author 	= 'Ryan Dingle';
	public appId  	= '259321127540805';
	public adminId  = '706724252756077';
	public appType 	= 'website';
	public siteName = 'RLD Webshop - Full Stack Web Developer';

    constructor(private meta: Meta, private title: Title) { }

    add(title: any , keywords: any, description: any, summary:any, url: any, image: any, website: any = this.appType) {
    	/*search engine*/
    	this.meta.addTag({ name: 'author', content: this.author });
    	this.meta.addTag({ name: 'keywords', content: keywords });
    	this.meta.addTag({ name: 'description', content: description });
    	this.meta.addTag({ name: 'image', content: image });

    	/*open graph for fb, etc...*/
    	this.meta.addTag({ name: 'og:title', content: title });
    	this.meta.addTag({ name: 'og:description', content: description });
    	this.meta.addTag({ name: 'og:image', content: image });
    	this.meta.addTag({ name: 'og:url', content: url });
    	this.meta.addTag({ name: 'og:site_name', content: this.siteName });
    	this.meta.addTag({ name: 'og:locale', content: this.lang });
    	this.meta.addTag({ name: 'og:admins', content: this.adminId });
    	this.meta.addTag({ name: 'og:app_id', content: this.appId });
    	this.meta.addTag({ name: 'og:type', content: website });

    	/*twitter*/
    	this.meta.addTag({ name: 'twitter:card', content: summary });
    	this.meta.addTag({ name: 'twitter:title', content: title });
    	this.meta.addTag({ name: 'twitter:description', content: description });
    	this.meta.addTag({ name: 'twitter:site', content: this.siteName });
    	this.meta.addTag({ name: 'twitter:creator', content: this.author });
    	this.meta.addTag({ name: 'twitter:image:src', content: image });
    }

    update(data: any) {
    	this.meta.updateTag(data);
    }

    setTitle(title) {
        this.title.setTitle(title);
    }

    updateSite(site) {
        this.meta.addTag({ name: 'og:site_name', content: site });
        this.meta.addTag({ name: 'twitter:site', content: site });
    }

    updateTitle(title) {
        //this.meta.updateTag({ 'itemprop': 'name', content: title });
        this.meta.updateTag({ name: 'og:title', content: title });
        this.meta.updateTag({ name: 'twitter:title', content: title });
    }

    updateAuthor(creator) {
        this.meta.updateTag({ name: 'twitter:creator', content: creator });
        this.meta.updateTag({ name: 'author', content: creator });
    }

    updateKeyword(keyword) {
        this.meta.updateTag({ name: 'keywords', content: keyword });
    }

    updateDescription(description) {
        this.meta.updateTag({ name: 'twitter:card', content: description });
        this.meta.updateTag({ name: 'description', content: description });
        //this.meta.updateTag({ 'itemprop': 'description', content: description });
        this.meta.updateTag({ name: 'og:description', content: description });
        this.meta.updateTag({ name: 'twitter:description', content: description });
    }

    updateImage(image) {
        //this.meta.updateTag({ 'itemprop': 'image', content: image });
        this.meta.updateTag({ name: 'image', content: image });
        this.meta.updateTag({ name: 'og:image', content: image });
        this.meta.updateTag({ name: 'twitter:image:src', content: image });
    }

    updateSummary(summary){
        this.meta.updateTag({ name: 'twitter:card', content: summary });
    }

    get(property: any) {
    	this.meta.getTag(property);
    }

    remove(property: any) {
    	this.meta.removeTag(property);
    }
}