import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';


@Injectable()
export class AuthService {

  private islogin   = localStorage.getItem('token') ? true : false;

  constructor(private api: ApiService) {}

  public login(data: any) {
    return this.api.post('/auth/login', data);
  }

  public register(data: any) {
    return this.api.post('/auth/register', data);
  }

  public logout() {
    return this.api.post('/auth/logout?token='+this.getToken(),{});
  }

  public getUser() {
    return this.api.get('/user/'+this.getUserToken().id);
  }

  public getStatus() {
      if(localStorage.getItem('token')) return true;
  }

  public getToken() {
    return JSON.parse(localStorage.getItem('token'));
  }

  public getUserToken() {
    return JSON.parse(localStorage.getItem('user'));
  }

  public setLogin(user, token) {
    this.islogin = true;
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
  }

  public updateUserToken(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public setLogout() {
    this.islogin = false;
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }

  public isLoggedIn() {
    return this.islogin;
  }
}
