import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class PortfolioService {

    constructor(private api: ApiService) {}

    public list(data: any) {
        return this.api.listByPost('/project', data);
    }

    public get(id: any) {
        return this.api.get('/project'+id+'/get');
    }

    public getRecentPost() {
        return this.api.get('/project/recent-post');
    }

    public edit(id: any) {
        return this.api.get('/project/'+id+'/edit');
    }

    public delete(id: any) {
        return this.api.delete('/project/'+id+'/delete');
    }

    public store(data: any) {
        return this.api.post('/project/post', data);
    }

    public update(data: any, id: any) {
        return this.api.update('/project/'+id+'/update', data);
    }
}