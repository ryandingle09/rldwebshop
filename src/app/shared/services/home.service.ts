import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class HomeService {

    constructor(private api: ApiService) {}

    public store(data) {
      return this.api.post('/subscriber/post', data);
    }
}