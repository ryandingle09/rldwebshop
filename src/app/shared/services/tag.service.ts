import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class TagService {

    constructor(private api: ApiService) {}

    public list() {
        return this.api.list('/tag');
    }

    public list2(data: any) {
        return this.api.listByPost('/tag/list', data);
    }

    public edit(id: any) {
        return this.api.get('/tag/'+id+'/edit');
    }

    public delete(id: any) {
        return this.api.delete('/tag/'+id+'/delete');
    }

    public store(data: any) {
        return this.api.post('/tag/post', data);
    }

    public update(data: any, id: any) {
        return this.api.update('/tag/'+id+'/update', data);
    }
}