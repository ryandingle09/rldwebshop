import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class SiteService {
    
    constructor(private api: ApiService) {}

    public getSite() {
        return this.api.get('/site');
    }

    public getSocial() {
        return this.api.get('/social');
    }

    public update(data: any, id: any) {
        return this.api.update('/site/'+id+'/update', data);
    }

    public store(data: any) {
       return this.api.post('/site/post', data);
    }
}