import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';
import '../rxjs-operator';

@Injectable()
export class VisitorService {

    constructor(private api: ApiService) {}

    public store() {
        return this.api.post('/visitor/post', {'data': 'visitor'});
    }
}