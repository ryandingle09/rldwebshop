import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class NotificationService {

    constructor(private api: ApiService) {}

    public list(data) {
      	return this.api.listByPost('/notification', data);
    }

    public messages(data) {
      	return this.api.listByPost('/notification/messages', data);
    }

    public get(type, typeId) {
      	return this.api.get('/notification/'+type+'/'+typeId+'/get');
    }

    public read(id) {
    	return this.api.post('/notification/read', {'id': id});
    }

    public readAll() {
      return this.api.post('/notification/read-all', {'action': 'read-all'});
    }
}