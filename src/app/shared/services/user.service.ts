import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class UserService {

    constructor(private api: ApiService) {}

    public get(id) {
        return this.api.get('/user/'+id+'/get');
    }

    public update(id, data) {
        return this.api.post('/user/'+id+'/update', data);
    }
}