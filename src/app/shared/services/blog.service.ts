import { Injectable } from '@angular/core';
import { ApiService  }  from './api.service';

@Injectable()
export class BlogService {

    constructor(private api: ApiService) { }

    public list(data: any) {
        return this.api.listByPost('/blog', data);
    }

    public getRecentPost() {
        return this.api.list('/blog/recent-post');
    }

    public listByCategory(data, id) {
        return this.api.listByPost('/blog/'+id+'/post-by-category', data);
    }

    public getCategoryList() {
        return this.api.list('/blog/category/list');
    }

    public get(id: any) {
        return this.api.get('/blog/'+id+'/get');
    }

    public getBySlug(slug: any) {
        return this.api.get('/blog/'+slug+'/show');
    }

    public edit(id: any) {
        return this.api.get('/blog/'+id+'/edit');
    }

    public delete(id: any) {
        return this.api.delete('/blog/'+id+'/delete');
    }

    public store(data: any) {
        return this.api.post('/blog/post', data);
    }

    public update(data: any, id: any) {
        return this.api.post('/blog/'+id+'/update', data);
    }
}