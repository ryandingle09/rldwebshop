export class CounterModel {
	count: number;
	name: any;
	portfolios: any = 0;
	blogs: any = 0;
	articles: any = 0;
	subscribers: any = 0;
	visitors: any = 0;
}