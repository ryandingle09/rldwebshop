export class CmsModel {
	id: any;
	prefix: any;
	header_image: any;
	package_image: any;
	subscribe_image: any;
	heading: any;
	heading_description: any;
	subscribe_description: any;
	heading_background_color: any;
	package_background_color: any;
	services_background_color: any;
	title: any;
	date: any;
	description: any;
	image: any;
	created_at: any;
	updated_at: any;
}