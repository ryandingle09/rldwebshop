import { ModuleWithProviders, NgModule } from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { ContactComponent } from './contact.component';
import { SharedModule, CmsService, ContactService } from '../shared'; 

const Routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'contact',
    component: ContactComponent,
    data: {
      page: 'contact',
      title: 'RLD Webshop - Contact'
    }
  }
]);

@NgModule({
  imports: [
    Routing,
    SharedModule,
    NoopAnimationsModule
  ],
  declarations: [
    ContactComponent
  ],
  providers: [CmsService, ContactService]
})
export class ContactModule {}