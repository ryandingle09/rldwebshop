import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal/';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { 
  SiteModel, 
  SiteService, 
  SeoService,
  CmsModel,
  StorageService,
  Errors,
  ContactService
} from '../shared';

@Component({
  selector: 'app-contact"',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  public sitedata: SiteModel[];
  public contact: CmsModel[];
  public form: any;
  public image: any = '/assets/images/headers/index2.jpg';
  public errors: Errors;
  public modalRef: BsModalRef;
  public loading: any = false;

  constructor(
    private modalService: BsModalService,
    private site: SiteService,
    private seo: SeoService,
    private storage: StorageService,
    private contactService: ContactService
  ) { }

  ngOnInit() {
    this.contact    =  this.storage.get('contact');
    this.sitedata   =  this.storage.get('sitedata');
    this.image      = this.contact[0].image == null ? this.image : this.contact[0].image;
    /*SEO META UPDATE*/
    this.seo.updateKeyword('RLD Webshop, Contact, Location, Time');
    this.seo.updateTitle('RLD Webshop - Contact');
    this.seo.updateDescription('RLD Webshop Contact Page');
    this.seo.updateSummary('RLD Webshop Contact Page');
    this.seo.updateAuthor('Ryan L. Dingle');
    this.seo.updateImage(this.image);
    this.createForm();
  }


  createForm() {
    this.form = new FormGroup({
      name: new FormControl('',[Validators.required]),
      email: new FormControl('',[Validators.required, Validators.email]),
      message: new FormControl('',[Validators.required]),
    });
  }

  public save(template: TemplateRef<any>) {
    this.loading = true;
    let data: FormData = new FormData();
    data.append('name', this.form.value.name);
    data.append('email', this.form.value.email);
    data.append('message', this.form.value.message);

    this.contactService.store(data).then(response => {
        if(response.message == 'success') {
          this.modalRef = this.modalService.show(template);
          this.loading = false;
          this.errors = new Errors;
        }
      },
      error => {
        this.errors = JSON.parse(error._body);
        this.loading = false;
    });
  }

}
