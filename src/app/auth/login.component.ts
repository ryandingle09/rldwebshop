import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { 
  AuthService,
  SeoService,
  Errors, 
  UserModel 
} from '../shared';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private router: Router,
    private seo: SeoService
    ) { 
  }

  public form: any;
  public errors: Errors;
  public users: UserModel;

  ngOnInit() {
    /*SEO META UPDATE*/
    this.seo.updateKeyword('RLD Webshop, Login, Authentication');
    this.seo.updateTitle('RLD Webshop - Login');
    this.seo.updateDescription('RLD Webshop Login Page');
    this.seo.updateSummary('RLD Webshop Login Page');
    this.seo.updateImage('/assets/images/headers/index2.jpg');
    
    this.createForm();
  }

  public createForm() {
    this.form = new FormGroup({
        email: new FormControl(),
        password: new FormControl()
    });
  }

  public login() {
    this.auth.login(this.form.value)
    .then(response => { 
        this.users = response.data;
        this.auth.setLogin(response.data, response.token);
        this.router.navigateByUrl('/admin');
      },
      err => {
        this.errors = JSON.parse(err._body);
      }
    );
  }

}
