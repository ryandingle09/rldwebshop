import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { 
  AuthService,
  SeoService
} from '../shared/services';
import {
  Errors
} from '../shared/models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit {

  constructor(
    private seo: SeoService,
    private auth: AuthService,
    private router: Router
    ) {
    if(this.auth.isLoggedIn()) this.router.navigate(['/admin']);
  }

  public form: any;
  public errors: Errors;

  ngOnInit() {
    /*SEO META UPDATE*/
    this.seo.updateKeyword('RLD Webshop, Register, Authentication fro owner only');
    this.seo.updateTitle('RLD Webshop - Register for owner');
    this.seo.updateDescription('RLD Webshop Register Page');
    this.seo.updateSummary('RLD Webshop Register Page');
    this.seo.updateImage('/assets/images/headers/index2.jpg');

    this.createForm();
  }

  public createForm() {
    this.form = new FormGroup({
        name: new FormControl(),
        email: new FormControl(),
        password: new FormControl(),
        password_confirmation: new FormControl(),
    });
  }

  public register() {
    this.auth.register(this.form.value).then(response=>null, error=>this.errors = JSON.parse(error._body));
  }

}
