import { ModuleWithProviders, NgModule } from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule }          from '@angular/forms';
import { MetaComponent } from './meta/meta.component';
import { FrontendComponent } from './frontend/frontend.component';
import { 
  SharedModule, 
  AuthGuard,
  CmsService 
} from '../shared'; 
import { PaginationModule, AccordionModule } from 'ngx-bootstrap';
import { NgSelectizeModule } from 'ng-selectize';
import { SimpleNotificationsModule } from 'angular2-notifications';

const Routing: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'meta',
    component: MetaComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'meta',
      title: 'RLD Webshop - Site Meta Identity'
    }
  },
  {
    path: 'frontend',
    component: FrontendComponent,
    canActivate: [AuthGuard],
    data: {
      page: 'frontend',
      title: 'RLD Webshop - My Frontend Settings'
    }
  }
]);

@NgModule({
  imports: [
    Routing,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectizeModule,
    PaginationModule.forRoot(),
    AccordionModule .forRoot(),
    SimpleNotificationsModule.forRoot(),
    NoopAnimationsModule
  ],
  declarations: [
  	FrontendComponent,
  	MetaComponent
  ],
  providers: [AuthGuard, CmsService]
})
export class SiteModule {}