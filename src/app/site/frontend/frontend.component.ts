import { 
  Component, 
  OnInit, 
  TemplateRef  
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal/';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  CmsService,
  CmsModel,
  Errors,
  AlertService
} from '../../shared';

@Component({
  selector: 'app-frontend',
  templateUrl: './frontend.component.html',
})
export class FrontendComponent implements OnInit {

  public modalRef: BsModalRef;
  public errors: Errors;
  public loading: boolean = false;
  public active: string = 'home';
  public homeForm;
  public workForm;
  public aboutForm;
  public contactForm;
  public blogForm;
  public serviceForm;
  public historyForm;
  public skillForm;
  public action: any = 'post';
  public show: boolean = false;
  public formElem: any = 'home';
  public name: any;
  public id: any;
  public image_f1: any = undefined;
  public image_f2: any = undefined;
  public image_f3: any = undefined;
  public home: CmsModel;
  public about: CmsModel;
  public blog: CmsModel;
  public portfolio: CmsModel;
  public contact: CmsModel;
  public service: CmsModel[];
  public skill: CmsModel[];
  public history: CmsModel[];
  public maxSize:number = 5;
  public bigTotalItems:number = 0;
  public bigCurrentPage = 1;
  public numPages:number = 5;
  public itemsPerPage: number = 10;
  public bigCurrenItems:number = 1;
  public offset:number = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage*this.itemsPerPage;
  public datas = {
    'value': '',
    'per_page' : this.itemsPerPage,
    'offset': this.offset
  };

  constructor(
    private modalService: BsModalService,
    private cms: CmsService,
    private notify: AlertService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.form();
    this.cms.listByPost(this.datas, this.formElem).then(response => { 
      if(this.formElem == 'service'){
        this.service = response[0].data;
        this.bigTotalItems = response[0].total;
        this.loading = false;
      }
      else if(this.formElem == 'skill'){
        this.skill = response[0].data;
        this.bigTotalItems = response[0].total;
        this.loading = false;
      }
      else if(this.formElem == 'history'){
        this.history = response[0].data;
        this.bigTotalItems = response[0].total;
        this.loading = false;
      }
      else if(this.formElem == 'home')
      {
        this.loading = false;
        if(response[0]) {
          this.action = 'update';
          this.id = response[0].id;
          this.home = response[0];
          this.homeForm.patchValue({
            heading: response[0].heading,
            heading_description: response[0].heading_description,
            subscribe_description: response[0].subscribe_description,
            heading_background_color: response[0].heading_background_color,
            services_background_color: response[0].services_background_color,
            package_background_color: response[0].package_background_color,
          });
        }
      }
      else if(this.formElem == 'about')
      {
        this.loading = false;
        if(response[0]) {
          this.action = 'update';
          this.id = response[0].id;
          this.about = response[0];
          this.aboutForm.patchValue({
            title: response[0].title,
            description: response[0].description,
          });
        }
      }
      else if(this.formElem == 'blog')
      {
        this.loading = false;
        if(response[0]) {
          this.action = 'update';
          this.id = response[0].id;
          this.blog = response[0];
          this.blogForm.patchValue({
            title: response[0].title,
            description: response[0].description,
          });
        }
      }
      else if(this.formElem == 'portfolio')
      {
        this.loading = false;
        if(response[0]) {
          this.action = 'update';
          this.id = response[0].id;
          this.portfolio = response[0];
          this.workForm.patchValue({
            title: response[0].title,
            description: response[0].description,
          });
        }
      }
      else if(this.formElem == 'contact')
      {
        this.loading = false;
        if(response[0]) {
          this.action = 'update';
          this.id = response[0].id;
          this.contact = response[0];
          this.contactForm.patchValue({
            title: response[0].title,
            description: response[0].description,
          });
        }
      }
    });
  }

  public create(form, type, action, id = null) {
    let act = (type == 'on') ? true : false;
    this.show = act;
    this.formElem = form;
    this.action = action;
    this.id = id;

    if(action == 'edit') {
      this.edit(form, id);
    }
  }

  public cancel(form,t) {
    this.ngOnInit();
    this.show = false;
  }

  public save(action, form) {
    this.loading = true;
    let data: FormData = new FormData();
    if(form == 'home') {
      this.homeForm.value.header_image    = this.image_f1;
      this.homeForm.value.package_image   = this.image_f2;
      //this.homeForm.value.subscribe_image = this.image_f3;
      data.append('page', 'home');
      data.append('header_image', this.image_f1 == undefined ? '' : this.image_f1);
      data.append('package_image', this.image_f2 == undefined ? '' : this.image_f2);
      //data.append('subscribe_image', this.image_f3 == undefined ? '' : this.image_f3);
      data.append('heading', this.homeForm.value.heading == null ? '' : this.homeForm.value.heading);
      data.append('heading_description', this.homeForm.value.heading_description == null ? '' : this.homeForm.value.heading_description);
      data.append('subscribe_description', this.homeForm.value.subscribe_description == null ? '' : this.homeForm.value.subscribe_description);
      data.append('heading_background_color', this.homeForm.value.heading_background_color == null ? '' : this.homeForm.value.heading_background_color);
      data.append('package_background_color', this.homeForm.value.package_background_color == null ? '' : this.homeForm.value.package_background_color);
      data.append('services_background_color', this.homeForm.value.services_background_color == null ? '' : this.homeForm.value.services_background_color);
    }else if(form == 'contact') {
      data.append('page', 'contact');
      this.contactForm.value.image    = this.image_f1;
      data.append('title', this.contactForm.value.title == null ? '' : this.contactForm.value.title);
      data.append('description', this.contactForm.value.description == null ? '' : this.contactForm.value.description);
      data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
    }else if(form == 'portfolio') {
      data.append('page', 'portfolio');
      this.workForm.value.image    = this.image_f1;
      data.append('title', this.workForm.value.title == null ? '' : this.workForm.value.title);
      data.append('description', this.workForm.value.description == null ? '' : this.workForm.value.description);
      data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
    }else if(form == 'blog') {
      data.append('page', 'blog');
      this.blogForm.value.image    = this.image_f1;
      data.append('title', this.blogForm.value.title == null ? '' : this.blogForm.value.title);
      data.append('description', this.blogForm.value.description == null ? '' : this.blogForm.value.description);
      data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
    }else if(form == 'about') {
      data.append('page', 'about');
      this.aboutForm.value.image    = this.image_f1;
      data.append('title', this.aboutForm.value.title == null ? '' : this.aboutForm.value.title);
      data.append('description', this.aboutForm.value.description == null ? '' : this.aboutForm.value.description);
      data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
    }else if(form == 'history') {
      data.append('page', 'history');
      data.append('title', this.historyForm.value.title == null ? '' : this.historyForm.value.title);
      data.append('description', this.historyForm.value.description == null ? '' : this.historyForm.value.description);
      data.append('date', this.historyForm.value.date == null ? '' : this.historyForm.value.date);
    }else if(form == 'skill') {
      data.append('page', 'skill');
      this.skillForm.value.image    = this.image_f1;
      data.append('title', this.skillForm.value.title == null ? '' : this.skillForm.value.title);
      data.append('description', this.skillForm.value.description == null ? '' : this.skillForm.value.description);
      data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
    }else if(form == 'service') {
      data.append('page', 'service');
      this.serviceForm.value.image    = this.image_f1;
      data.append('title', this.serviceForm.value.title == null ? '' : this.serviceForm.value.title);
      data.append('description', this.serviceForm.value.description == null ? '' : this.serviceForm.value.description);
      data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
    }

    if(action == 'update' && this.id !== ''){
      this.loading = true;
      this.cms.update(data, this.id).then(response => {
          this.ngOnInit();
          this.show = false; 
          this.loading = false;
          this.notify.alert(response.success, 'success');
        }, 
        error => {
          this.notify.alert('Unable to store data.', 'error');
          this.errors = JSON.parse(error._body);
      });
    }else{
      this.cms.store(data).then(response => {
          if(this.formElem == 'skill' || this.formElem == 'history' || this.formElem == 'service') {
            this.ngOnInit();
            this.show = false; 
          }else{
            if(this.active == 'home'){
              this.id = response.data.id;
              this.action = 'update';
              this.home = response.data;
            }else if(this.active == 'portfolio'){
              this.id = response.data.id;
              this.action = 'update';
              this.portfolio = response.data;
            }else if(this.active == 'about'){
              this.id = response.data.id;
              this.action = 'update';
              this.about = response.data;
            }else if(this.active == 'blog'){
              this.id = response.data.id;
              this.action = 'update';
              this.blog = response.data;
            }else if(this.active == 'contact'){
              this.id = response.data.id;
              this.action = 'update';
              this.contact = response.data;
            }
          }
          this.notify.alert(response.success, 'success');
          this.loading = false;
        }, 
        error => {
          this.notify.alert('Unable to store data.', 'error');
          this.errors = JSON.parse(error._body);
      });
    }
  }

  public edit(form, id) {
    this.id = id;
    this.action = 'update';
    this.cms.get(id).then(response => {
      if(form == 'service'){
        this.service = response[0];
        this.serviceForm.patchValue({
          title: response[0].title,
          description: response[0].description,
        });
      }
      if(form == 'skill'){
        this.skill = response[0];
        this.skillForm.patchValue({
          title: response[0].title,
          description: response[0].description,
        });
      }
      if(form == 'history'){
        this.history = response[0];
        this.historyForm.patchValue({
          title: response[0].title,
          date: response[0].date,
          description: response[0].description,
        });
      }
    });
  }

  public deleteModal(name, id, template: TemplateRef<any>) {
    this.name = name;
    this.id = id;
    this.modalRef = this.modalService.show(template);
  }

  public delete(form, id) {
    this.loading = true;
    this.cms.delete(id).then(response => {
        if(form == 'service'){
          this.service = response;
        }
        else if(form == 'skill'){
          this.skill = response;
        }
        else if(form == 'history'){
          this.history = response;
        }

        this.ngOnInit();
        this.modalRef.hide(); 
        this.notify.alert(response.success, 'success');
      },
      error => {
        this.notify.alert('Unable to delete item.', 'error');
        this.errors = JSON.parse(error._body);
      }
    );
  }

  public getFile(event, input) {
    let file = event.target.files[0];
    if(input == 'image_f1') this.image_f1 = file;
    if(input == 'image_f2') this.image_f2 = file;
    if(input == 'image_f3') this.image_f3 = file;
  }

  public form() {
    this.homeForm = new FormGroup({
      heading: new FormControl(),
      heading_description: new FormControl(),
      subscribe_description: new FormControl(),
      header_image: new FormControl(),
      package_image: new FormControl(),
      //subscribe_image: new FormControl(),
      heading_background_color: new FormControl(),
      services_background_color: new FormControl(),
      package_background_color: new FormControl(),
    });

    this.serviceForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      image: new FormControl(),
      description: new FormControl('', [Validators.required]),
    });

    this.blogForm = new FormGroup({
      image: new FormControl(),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });

    this.workForm = new FormGroup({
      image: new FormControl(),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });

    this.contactForm = new FormGroup({
      image: new FormControl(),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });

    this.aboutForm = new FormGroup({
      image: new FormControl(),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });

    this.skillForm = new FormGroup({
      image: new FormControl(),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });

    this.historyForm = new FormGroup({
      date: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });
  }

  public setElemForm(elem) {
    this.formElem = elem;
    this.errors = new Errors;
    this.show = false;
    this.datas.value = '';
    this.ngOnInit();
  }

  public search(value) {
    this.datas.value = value;
    this.ngOnInit();
  }

  public refresh() {
    this.ngOnInit();
  }

  public perPage(value) {
    this.itemsPerPage = value;
    this.ngOnInit();
  }
 
  public pageChanged(event:any):void {
    this.bigCurrentPage = event.page;
    this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage*this.itemsPerPage;
    this.ngOnInit();
  }

  public setActive(tab) {
    this.errors = new Errors;
    this.active = tab;
    this.formElem = tab;
    this.show = false;
    this.datas.value = '';
    this.id = '';
    this.action = 'post';
    if(tab == 'home' || tab == 'portfolio' || tab == 'blog' || tab == 'contact' || tab =='about') {
      this.ngOnInit();
    }
  }
}
