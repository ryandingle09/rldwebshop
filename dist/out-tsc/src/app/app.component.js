"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
require("rxjs/add/operator/filter");
var common_1 = require("@angular/common");
var shared_1 = require("./shared");
var AppComponent = /** @class */ (function () {
    function AppComponent(router, cms, auth, seo, storage, visit, platformId) {
        var _this = this;
        this.router = router;
        this.cms = cms;
        this.auth = auth;
        this.seo = seo;
        this.storage = storage;
        this.visit = visit;
        this.platformId = platformId;
        this.lang = 'en_US';
        this.author = 'Ryan Dingle';
        this.appId = '234234';
        this.adminId = '234234';
        this.appType = 'website';
        this.siteName = 'RLD Webshop - Full Stack Web Developer';
        router.events
            .filter(function (event) { return event instanceof router_1.NavigationEnd; })
            .subscribe(function (event) {
            if (common_1.isPlatformBrowser(_this.platformId)) {
                window.scroll(0, 0);
            }
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        this.visit.store();
        this.seo.update({ name: 'twitter:creator', content: this.author });
        this.seo.update({ name: 'author', content: this.author });
        this.seo.update({ name: 'twitter:site', content: this.siteName });
        this.seo.update({ name: 'og:site_name', content: this.siteName });
        this.seo.update({ name: 'og:locale', content: this.lang });
        this.seo.update({ name: 'og:admins', content: this.adminId });
        this.seo.update({ name: 'og:app_id', content: this.appId });
        this.seo.update({ name: 'og:type', content: 'website' });
        this.initialize();
    };
    AppComponent.prototype.initialize = function () {
        var _this = this;
        this.cms.getPageCms('home').then(function (response) {
            _this.sitedata = response.site;
            _this.socialdata = response.social;
            _this.home = response.home;
            _this.service = response.service;
            _this.about = response.about;
            _this.blog = response.blog;
            _this.portfolio = response.portfolio;
            _this.contact = response.contact;
            _this.skill = response.skill;
            _this.history = response.history;
            _this.works = response.project;
            _this.storage.set('sitedata', response.site);
            _this.storage.set('socialdata', response.social);
            _this.storage.set('home', response.home);
            _this.storage.set('service', response.service);
            _this.storage.set('about', response.about);
            _this.storage.set('blog', response.blog);
            _this.storage.set('portfolio', response.portfolio);
            _this.storage.set('works', response.project);
            _this.storage.set('contact', response.contact);
            _this.storage.set('skill', response.skill);
            _this.storage.set('history', response.history);
        });
        /*this.site.getSocial().then(response => {
            this.socialdata = response;
            this.storage.set('socialdata', response);
        });*/
        /*this.router.events.subscribe((res) => {
          if (res instanceof RoutesRecognized) {
            let route = res.state.root.firstChild;
            let page = route.data.page;
            if(page == 'register' || page == 'login' || page == 'home' || page == 'about' || page == 'works' || page == 'contact' || page == 'blog' || page == 'blogdetail') {
                this.site.getSocial().then(response => {
                    this.socialdata = response;
                    this.storage.set('socialdata', response);
                });
            }
          }
        });*/
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html'
        }),
        __param(6, core_1.Inject(core_1.PLATFORM_ID)),
        __metadata("design:paramtypes", [router_1.Router,
            shared_1.CmsService,
            shared_1.AuthService,
            shared_1.SeoService,
            shared_1.StorageService,
            shared_1.VisitorService,
            Object])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map