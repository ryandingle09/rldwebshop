"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var shared_1 = require("../shared");
var WorksComponent = /** @class */ (function () {
    function WorksComponent(router, route, portfolio, seo, storage) {
        this.router = router;
        this.route = route;
        this.portfolio = portfolio;
        this.seo = seo;
        this.storage = storage;
        this.maxSize = 5;
        this.bigTotalItems = 0;
        this.bigCurrentPage = 1;
        this.numPages = 5;
        this.itemsPerPage = 12;
        this.bigCurrenItems = 1;
        this.projects = [];
        this.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.datas = {
            'value': '',
            'per_page': this.itemsPerPage,
            'offset': this.offset
        };
        this.image = '/assets/images/featured2.png';
    }
    WorksComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.work = this.storage.get('portfolio');
        this.image = this.work[0].image == null ? this.image : this.work[0].image;
        /*SEO META UPDATE*/
        this.seo.updateTitle('RLD Webshop - Works/Portfolio');
        this.seo.updateAuthor('Ryan L. Dingle');
        this.seo.updateSummary('RLD Webshop Portfolio, Browse portfolio');
        this.seo.updateDescription('RLD Webshop Portfolio, Browse portfolio');
        this.seo.updateKeyword('Works, Projects, Portfolio');
        this.seo.updateImage(this.image);
        this.portfolio.list(this.datas).then(function (response) {
            _this.projects = response[0].data;
            _this.bigTotalItems = response[0].total;
        });
        this.router.events.subscribe(function (res) {
            if (res instanceof router_1.RoutesRecognized) {
                var route = res.state.root.firstChild;
                var headtitle = route.data.title;
            }
        });
    };
    WorksComponent.prototype.perPage = function (value) {
        this.itemsPerPage = value;
        this.ngOnInit();
    };
    WorksComponent.prototype.pageChanged = function (event) {
        this.bigCurrentPage = event.page;
        this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.ngOnInit();
    };
    WorksComponent = __decorate([
        core_1.Component({
            selector: 'app-works',
            templateUrl: './works.component.html',
        }),
        __metadata("design:paramtypes", [router_1.Router,
            router_1.ActivatedRoute,
            shared_1.PortfolioService,
            shared_1.SeoService,
            shared_1.StorageService])
    ], WorksComponent);
    return WorksComponent;
}());
exports.WorksComponent = WorksComponent;
//# sourceMappingURL=works.component.js.map