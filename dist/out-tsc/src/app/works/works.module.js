"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var router_1 = require("@angular/router");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var works_component_1 = require("./works.component");
var works_detail_component_1 = require("./works-detail.component");
var ng2_awesome_disqus_1 = require("ng2-awesome-disqus");
var ngx_sharebuttons_1 = require("ngx-sharebuttons");
var shared_1 = require("../shared");
var Routing = router_1.RouterModule.forChild([
    {
        path: 'works',
        component: works_component_1.WorksComponent,
        data: {
            page: 'works',
            title: 'RLD Webshop - Works/Portfolio'
        }
    },
    {
        path: 'works/:id',
        component: works_detail_component_1.WorksDetailComponent,
        data: {
            page: 'works',
            title: 'RLD Webshop - Work Details'
        }
    }
]);
var WorksModule = /** @class */ (function () {
    function WorksModule() {
    }
    WorksModule = __decorate([
        core_1.NgModule({
            imports: [
                Routing,
                shared_1.SharedModule,
                ngx_bootstrap_1.PaginationModule.forRoot(),
                ng2_awesome_disqus_1.DisqusModule,
                ngx_sharebuttons_1.ShareButtonsModule.forRoot(),
                animations_1.NoopAnimationsModule
            ],
            declarations: [
                works_component_1.WorksComponent,
                works_detail_component_1.WorksDetailComponent
            ],
            providers: [
                shared_1.PortfolioService
            ]
        })
    ], WorksModule);
    return WorksModule;
}());
exports.WorksModule = WorksModule;
//# sourceMappingURL=works.module.js.map