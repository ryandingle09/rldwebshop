"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var services_1 = require("../shared/services");
var WorksDetailComponent = /** @class */ (function () {
    function WorksDetailComponent(project, route, router, seo) {
        this.project = project;
        this.route = route;
        this.router = router;
        this.seo = seo;
        this.id = this.route.snapshot.params['id'];
        this.pageId = '/works/' + this.id;
    }
    WorksDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.project.get(this.id).then(function (response) {
            _this.projects = response[0];
            _this.seo.setTitle('Ryan Dingle - ' + _this.projects.title);
        });
        this.project.getRecentPost().then(function (response) { return _this.recentWorks = response; });
    };
    WorksDetailComponent.prototype.navigate = function (id) {
        var _this = this;
        this.route.params.subscribe(function (res) {
            _this.router.navigate(['works/', id]).then(function (res) {
                _this.pageId = '/works/' + id;
                _this.project.get(_this.id).then(function (response) {
                    _this.projects = response[0];
                    _this.seo.setTitle('Ryan Dingle - ' + _this.projects.title);
                });
            });
        });
    };
    WorksDetailComponent = __decorate([
        core_1.Component({
            selector: 'app-works-detail',
            templateUrl: './works-detail.component.html'
        }),
        __metadata("design:paramtypes", [services_1.PortfolioService,
            router_1.ActivatedRoute,
            router_1.Router,
            services_1.SeoService])
    ], WorksDetailComponent);
    return WorksDetailComponent;
}());
exports.WorksDetailComponent = WorksDetailComponent;
//# sourceMappingURL=works-detail.component.js.map