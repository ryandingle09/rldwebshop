"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/http");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var core_2 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var app_component_1 = require("./app.component");
var ngx_sharebuttons_1 = require("ngx-sharebuttons");
var angular2_notifications_1 = require("angular2-notifications");
//import 'web-animations-js';
var shared_1 = require("./shared");
var home_module_1 = require("./home/home.module");
var blog_module_1 = require("./blog/blog.module");
var about_module_1 = require("./about/about.module");
var contact_module_1 = require("./contact/contact.module");
var works_module_1 = require("./works/works.module");
var admin_module_1 = require("./admin/admin.module");
var auth_module_1 = require("./auth/auth.module");
var account_module_1 = require("./account/account.module");
var site_module_1 = require("./site/site.module");
var apps_module_1 = require("./apps/apps.module");
var rootRouting = router_1.RouterModule.forRoot([], { useHash: false });
var AppModule = /** @class */ (function () {
    function AppModule(platformId, appId) {
        this.platformId = platformId;
        this.appId = appId;
        var platform = common_1.isPlatformBrowser(platformId) ?
            'on the browser' : 'in the server';
        console.log("Running " + platform + " with appId=" + appId);
    }
    AppModule = __decorate([
        core_2.NgModule({
            declarations: [
                app_component_1.AppComponent,
                shared_1.FooterComponent,
                shared_1.HeaderComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule.withServerTransition({ appId: 'rldwebshopv5' }),
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                shared_1.SharedModule,
                home_module_1.HomeModule,
                blog_module_1.BlogModule,
                about_module_1.AboutModule,
                contact_module_1.ContactModule,
                works_module_1.WorksModule,
                admin_module_1.AdminModule,
                auth_module_1.AuthModule,
                site_module_1.SiteModule,
                account_module_1.AccountModule,
                apps_module_1.AppsModule,
                rootRouting,
                ngx_bootstrap_1.ModalModule.forRoot(),
                ngx_sharebuttons_1.ShareButtonsModule.forRoot(),
                http_1.HttpModule,
                angular2_notifications_1.SimpleNotificationsModule.forRoot(),
                animations_1.NoopAnimationsModule,
                animations_1.BrowserAnimationsModule
            ],
            providers: [platform_browser_1.Title, shared_1.SeoService, shared_1.ApiService, shared_1.StorageService, shared_1.VisitorService],
            bootstrap: [app_component_1.AppComponent]
        }),
        __param(0, core_1.Inject(core_1.PLATFORM_ID)),
        __param(1, core_1.Inject(core_1.APP_ID)),
        __metadata("design:paramtypes", [Object, String])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map