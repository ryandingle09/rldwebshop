"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _1 = require("ngx-bootstrap/modal/");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var shared_1 = require("../shared");
var models_1 = require("../shared/models");
var services_1 = require("../shared/services");
var AdminComponent = /** @class */ (function () {
    function AdminComponent(modalService, blog, portfolio, tag, category, auth, router, notify, counter, platformId) {
        this.modalService = modalService;
        this.blog = blog;
        this.portfolio = portfolio;
        this.tag = tag;
        this.category = category;
        this.auth = auth;
        this.router = router;
        this.notify = notify;
        this.counter = counter;
        this.platformId = platformId;
        this.config = shared_1.SelectMultiConfig;
        this.placeholder1 = 'Add Category';
        this.placeholder2 = 'Add Tag';
        this.active = 'portfolio';
        this.showForm = 0;
        this.maxSize = 5;
        this.bigTotalItems = 0;
        this.bigCurrentPage = 1;
        this.numPages = 5;
        this.itemsPerPage = 10;
        this.bigCurrenItems = 1;
        this.project = [];
        this.post = [];
        this.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.datas = {
            'value': '',
            'per_page': this.itemsPerPage,
            'offset': this.offset
        };
        this.isBrowser = common_1.isPlatformBrowser(platformId);
    }
    AdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.createForm2();
        this.createForm3();
        this.counter.get().then(function (response) { return _this.count = response; });
        if (this.active == 'portfolio') {
            this.portfolio.list(this.datas).then(function (response) {
                _this.project = response[0].data;
                _this.bigTotalItems = response[0].total;
            });
        }
        else {
            this.blog.list(this.datas).then(function (response) {
                _this.post = response[0].data;
                _this.bigTotalItems = response[0].total;
            });
        }
    };
    AdminComponent.prototype.create = function (action, title, id) {
        var _this = this;
        this.fValue = undefined;
        this.name = title;
        this.id = id;
        this.action = action;
        if (action == 'edit' && title == 'Portfolio') {
            this.portfolio.edit(id).then(function (response) {
                var data = response;
                _this.value1 = response.categories;
                _this.form.patchValue({
                    title: data.data.title,
                    slug: data.data.slug,
                    description: data.data.description,
                    category: response.categories,
                    link: data.data.link,
                    status: data.data.status,
                });
            });
        }
        else if (action == 'edit' && title == 'Blog') {
            this.blog.edit(id).then(function (response) {
                var data = response;
                _this.value1 = response.categories;
                _this.value2 = response.tags;
                _this.form3.patchValue({
                    title: data.data.title,
                    slug: data.data.slug,
                    body: data.data.body,
                    category: response.categories,
                    tag: response.tags,
                    status: data.data.status,
                });
            });
        }
        else {
            this.data = {};
            this.value1 = [];
            this.value2 = [];
        }
        this.showForm = 1;
    };
    AdminComponent.prototype.hide = function () {
        this.showForm = 0;
        this.form3.patchValue({
            title: '',
            slug: '',
            body: '',
        });
        this.form.patchValue({
            title: '',
            slug: '',
            description: '',
            link: '',
        });
    };
    AdminComponent.prototype.search = function (value) {
        this.datas.value = value;
        this.ngOnInit();
    };
    AdminComponent.prototype.refresh = function () {
        this.ngOnInit();
    };
    AdminComponent.prototype.perPage = function (value) {
        this.itemsPerPage = value;
        this.ngOnInit();
    };
    AdminComponent.prototype.pageChanged = function (event) {
        this.bigCurrentPage = event.page;
        this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.ngOnInit();
    };
    AdminComponent.prototype.setActive = function (tab) {
        this.errors = new models_1.Errors;
        this.active = tab;
        this.showForm = false;
        this.datas.value = '';
        this.ngOnInit();
    };
    AdminComponent.prototype.deleteModal = function (name, id, template) {
        this.name = name;
        this.id = id;
        this.modalRef = this.modalService.show(template);
    };
    AdminComponent.prototype.delete = function (name, id) {
        var _this = this;
        if (name == 'Portfolio') {
            this.portfolio.delete(id).then(function (response) {
                _this.ngOnInit();
                if (_this.isBrowser) {
                    _this.modalRef.hide();
                    _this.notify.alert(response.success, 'info');
                }
            }, function (error) {
                _this.errors = JSON.parse(error._body);
            });
        }
        else {
            this.blog.delete(id).then(function (response) {
                _this.ngOnInit();
                if (_this.isBrowser) {
                    _this.modalRef.hide();
                    _this.notify.alert(response.success, 'info');
                }
            }, function (error) {
                _this.errors = JSON.parse(error._body);
            });
        }
    };
    AdminComponent.prototype.openModal2 = function (name, template) {
        this.title2 = name;
        if (this.isBrowser) {
            this.modalRef2 = this.modalService.show(template);
        }
        this.createForm2();
    };
    AdminComponent.prototype.createForm = function () {
        var _this = this;
        this.category.list().then(function (response) { return _this.categories = response; });
        this.form = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            slug: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required]),
            link: new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.pattern("http?://.+|https?://.+")]),
            image: new forms_1.FormControl(),
            category: new forms_1.FormControl('', [forms_1.Validators.required]),
            status: new forms_1.FormControl('published', [forms_1.Validators.required])
        });
    };
    AdminComponent.prototype.createForm2 = function () {
        this.form2 = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required]),
            status: new forms_1.FormControl('published', [forms_1.Validators.required]),
        });
    };
    AdminComponent.prototype.createForm3 = function () {
        var _this = this;
        this.category.list().then(function (response) { return _this.categories = response; });
        this.tag.list().then(function (response) { return _this.tags = response; });
        this.form3 = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            slug: new forms_1.FormControl('', [forms_1.Validators.required]),
            body: new forms_1.FormControl('', [forms_1.Validators.required]),
            image: new forms_1.FormControl(),
            tag: new forms_1.FormControl('', [forms_1.Validators.required]),
            category: new forms_1.FormControl('', [forms_1.Validators.required]),
            status: new forms_1.FormControl('published', [forms_1.Validators.required])
        });
    };
    AdminComponent.prototype.getFile = function (event) {
        var file = event.target.files[0];
        this.fValue = file;
    };
    AdminComponent.prototype.save2 = function (name) {
        var _this = this;
        var data = this.form2.value;
        if (name == 'Category') {
            this.category.store(data).then(function (response) {
                _this.category.list().then(function (response) { return _this.categories = response; });
                if (_this.isBrowser) {
                    _this.modalRef2.hide();
                    _this.notify.alert(response.success, 'success');
                }
            }, function (error) { return _this.errors = JSON.parse(error._body); });
        }
        else {
            this.tag.store(data).then(function (response) {
                if (_this.isBrowser) {
                    _this.modalRef2.hide();
                    _this.notify.alert(response.success, 'success');
                }
                _this.tag.list().then(function (response) { return _this.tags = response; });
            }, function (error) { return _this.errors = JSON.parse(error._body); });
        }
    };
    AdminComponent.prototype.save = function (type) {
        var _this = this;
        this.form.value.realImage = this.fValue;
        var data = new FormData();
        if (this.name == 'Portfolio') {
            data.append('title', this.form.value.title == null ? '' : this.form.value.title);
            data.append('slug', this.form.value.slug == null ? '' : this.form.value.slug);
            data.append('description', this.form.value.description == null ? '' : this.form.value.description);
            data.append('link', this.form.value.link == null ? '' : this.form.value.link);
            data.append('category', this.form.value.category == null ? '' : this.form.value.category);
            data.append('status', this.form.value.status == null ? '' : this.form.value.status);
        }
        if (this.name == 'Blog') {
            data.append('title', this.form3.value.title == null ? '' : this.form3.value.title);
            data.append('slug', this.form3.value.slug == null ? '' : this.form3.value.slug);
            data.append('body', this.form3.value.body == null ? '' : this.form3.value.body);
            data.append('category', this.form3.value.category == null ? '' : this.form3.value.category);
            data.append('tag', this.form3.value.tag == null ? '' : this.form3.value.tag);
            data.append('status', this.form3.value.status == null ? '' : this.form3.value.status);
        }
        data.append('photo', this.fValue !== undefined ? this.fValue : '');
        if (this.name == 'Portfolio') {
            if (type == 'post') { //post process
                this.portfolio.store(data).then(function (response) {
                    _this.ngOnInit();
                    _this.errors = response;
                    _this.value1 = [];
                    _this.value2 = [];
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) {
                    _this.errors = JSON.parse(error._body);
                });
            }
            else { //update process
                this.portfolio.update(data, this.id).then(function (response) {
                    _this.ngOnInit();
                    _this.errors = response;
                    _this.showForm = 0;
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) {
                    _this.errors = JSON.parse(error._body);
                });
            }
        }
        else {
            if (type == 'post') { //post process
                this.blog.store(data).then(function (response) {
                    _this.ngOnInit();
                    _this.errors = response;
                    _this.value1 = [];
                    _this.value2 = [];
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) {
                    _this.errors = JSON.parse(error._body);
                });
            }
            else { //update process
                this.blog.update(data, this.id).then(function (response) {
                    _this.ngOnInit();
                    _this.errors = response;
                    _this.showForm = 0;
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) {
                    _this.errors = JSON.parse(error._body);
                });
            }
        }
    };
    AdminComponent.prototype.slug = function (slug) {
        var word = slug.split(" ").join("-").toLowerCase();
        word = word.split("/").join("-").toLowerCase();
        word = word.split("+").join("-").toLowerCase();
        word = word.split(":").join("-").toLowerCase();
        word = word.split("#").join("-").toLowerCase();
        word = word.split("?").join("-").toLowerCase();
        word = word.split("%").join("-").toLowerCase();
        word = word.split("*").join("-").toLowerCase();
        word = word.split("=").join("-").toLowerCase();
        word = word.split("$").join("-").toLowerCase();
        word = word.split("&").join("-").toLowerCase();
        word = word.split("_").join("-").toLowerCase();
        word = word.split("~").join("-").toLowerCase();
        word = word.split("!").join("-").toLowerCase();
        word = word.split("`").join("-").toLowerCase();
        word = word.split("]").join("-").toLowerCase();
        word = word.split("^").join("-").toLowerCase();
        word = word.split("[").join("-").toLowerCase();
        word = word.split("'").join("-").toLowerCase();
        word = word.split(".").join("-").toLowerCase();
        word = word.split("—").join("-").toLowerCase();
        word = word.split("}").join("-").toLowerCase();
        word = word.split("{").join("-").toLowerCase();
        word = word.split("(").join("-").toLowerCase();
        word = word.split(")").join("-").toLowerCase();
        word = word.split("@").join("-").toLowerCase();
        word = word.split(">").join("-").toLowerCase();
        word = word.split("<").join("-").toLowerCase();
        word = word.split("|").join("-").toLowerCase();
        this.form.patchValue({ slug: word });
        this.form3.patchValue({ slug: word });
    };
    AdminComponent = __decorate([
        core_1.Component({
            selector: 'app-admin',
            templateUrl: './admin.component.html',
        }),
        __param(9, core_1.Inject(core_1.PLATFORM_ID)),
        __metadata("design:paramtypes", [_1.BsModalService,
            services_1.BlogService,
            services_1.PortfolioService,
            services_1.TagService,
            services_1.CategoryService,
            services_1.AuthService,
            router_1.Router,
            services_1.AlertService,
            services_1.CounterService,
            Object])
    ], AdminComponent);
    return AdminComponent;
}());
exports.AdminComponent = AdminComponent;
//# sourceMappingURL=admin.component.js.map