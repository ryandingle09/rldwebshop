"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var admin_component_1 = require("./admin.component");
var shared_1 = require("../shared");
var ng2_ckeditor_1 = require("ng2-ckeditor");
var services_1 = require("../shared/services");
var ng_selectize_1 = require("ng-selectize");
var angular2_notifications_1 = require("angular2-notifications");
var Routing = router_1.RouterModule.forChild([
    {
        path: 'admin',
        component: admin_component_1.AdminComponent,
        canActivate: [services_1.AuthGuard],
        data: {
            page: 'admin',
            title: 'RLD Webshop - Admin Dashboard'
        }
    }
]);
var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        core_1.NgModule({
            imports: [
                Routing,
                shared_1.SharedModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ngx_bootstrap_1.PaginationModule.forRoot(),
                ng_selectize_1.NgSelectizeModule,
                animations_1.NoopAnimationsModule,
                angular2_notifications_1.SimpleNotificationsModule.forRoot(),
                ng2_ckeditor_1.CKEditorModule
            ],
            declarations: [
                admin_component_1.AdminComponent,
            ],
            exports: [],
            providers: [
                services_1.SiteService,
                services_1.PortfolioService,
                services_1.BlogService,
                services_1.TagService,
                services_1.CategoryService,
                services_1.AuthGuard,
                services_1.AlertService,
                services_1.CounterService
            ],
            entryComponents: []
        })
    ], AdminModule);
    return AdminModule;
}());
exports.AdminModule = AdminModule;
//# sourceMappingURL=admin.module.js.map