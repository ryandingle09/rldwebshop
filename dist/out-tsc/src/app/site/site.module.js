"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var meta_component_1 = require("./meta/meta.component");
var frontend_component_1 = require("./frontend/frontend.component");
var shared_1 = require("../shared");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var ng_selectize_1 = require("ng-selectize");
var angular2_notifications_1 = require("angular2-notifications");
var Routing = router_1.RouterModule.forChild([
    {
        path: 'meta',
        component: meta_component_1.MetaComponent,
        canActivate: [shared_1.AuthGuard],
        data: {
            page: 'meta',
            title: 'RLD Webshop - Site Meta Identity'
        }
    },
    {
        path: 'frontend',
        component: frontend_component_1.FrontendComponent,
        canActivate: [shared_1.AuthGuard],
        data: {
            page: 'frontend',
            title: 'RLD Webshop - My Frontend Settings'
        }
    }
]);
var SiteModule = /** @class */ (function () {
    function SiteModule() {
    }
    SiteModule = __decorate([
        core_1.NgModule({
            imports: [
                Routing,
                shared_1.SharedModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ng_selectize_1.NgSelectizeModule,
                ngx_bootstrap_1.PaginationModule.forRoot(),
                ngx_bootstrap_1.AccordionModule.forRoot(),
                angular2_notifications_1.SimpleNotificationsModule.forRoot(),
                animations_1.NoopAnimationsModule
            ],
            declarations: [
                frontend_component_1.FrontendComponent,
                meta_component_1.MetaComponent
            ],
            providers: [shared_1.AuthGuard, shared_1.CmsService]
        })
    ], SiteModule);
    return SiteModule;
}());
exports.SiteModule = SiteModule;
//# sourceMappingURL=site.module.js.map