"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var services_1 = require("../../shared/services");
var MetaComponent = /** @class */ (function () {
    function MetaComponent(site, notify) {
        this.site = site;
        this.notify = notify;
        this.showForm = 0;
        this.id = '';
    }
    MetaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm();
        this.site.getSite().then(function (response) {
            _this.sitedata = response[0];
            _this.id = response[0].id;
        });
    };
    MetaComponent.prototype.getFile = function (event) {
        var file = event.target.files[0];
        this.photo = file;
    };
    MetaComponent.prototype.getFile2 = function (event) {
        var file = event.target.files[0];
        this.logo = file;
    };
    MetaComponent.prototype.hide = function () {
        this.showForm = 0;
    };
    MetaComponent.prototype.show = function () {
        this.showForm = 1;
        this.form.patchValue({
            title: this.sitedata.title,
            email: this.sitedata.email,
            tagline: this.sitedata.tagline,
            description: this.sitedata.description,
            contact: this.sitedata.contact,
            location: this.sitedata.location,
            owner: this.sitedata.owner
        });
    };
    MetaComponent.prototype.createForm = function () {
        this.form = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            email: new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.email]),
            tagline: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required]),
            contact: new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.pattern('[0-9]+')]),
            location: new forms_1.FormControl('', [forms_1.Validators.required]),
            owner: new forms_1.FormControl('', [forms_1.Validators.required]),
            image: new forms_1.FormControl(),
            logo: new forms_1.FormControl(),
        });
    };
    MetaComponent.prototype.save = function () {
        var _this = this;
        var data = new FormData();
        data.append('title', this.form.value.title == null ? '' : this.form.value.title);
        data.append('email', this.form.value.email == null ? '' : this.form.value.email);
        data.append('tagline', this.form.value.tagline == null ? '' : this.form.value.tagline);
        data.append('description', this.form.value.description == null ? '' : this.form.value.description);
        data.append('contact', this.form.value.contact == null ? '' : this.form.value.contact);
        data.append('location', this.form.value.location == null ? '' : this.form.value.location);
        data.append('owner', this.form.value.owner == null ? '' : this.form.value.owner);
        data.append('photo', this.photo == undefined ? '' : this.photo);
        data.append('photo2', this.logo == undefined ? '' : this.logo);
        if (this.id == '') {
            this.site.store(data).then(function (response) {
                _this.sitedata = response[0];
                _this.notify.alert('Successfully Saved.', 'success');
            }, function (error) {
                _this.errors = error._body;
            });
        }
        else {
            this.site.update(data, this.id).then(function (response) {
                _this.sitedata = response[0];
                _this.notify.alert('Successfully Updated.', 'success');
            }, function (error) {
                _this.errors = error._body;
            });
        }
    };
    MetaComponent = __decorate([
        core_1.Component({
            selector: 'app-meta',
            templateUrl: './meta.component.html',
        }),
        __metadata("design:paramtypes", [services_1.SiteService,
            services_1.AlertService])
    ], MetaComponent);
    return MetaComponent;
}());
exports.MetaComponent = MetaComponent;
//# sourceMappingURL=meta.component.js.map