"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var _1 = require("ngx-bootstrap/modal/");
var shared_1 = require("../../shared");
var FrontendComponent = /** @class */ (function () {
    function FrontendComponent(modalService, cms, notify) {
        this.modalService = modalService;
        this.cms = cms;
        this.notify = notify;
        this.loading = false;
        this.active = 'home';
        this.action = 'post';
        this.show = false;
        this.formElem = 'home';
        this.image_f1 = undefined;
        this.image_f2 = undefined;
        this.image_f3 = undefined;
        this.maxSize = 5;
        this.bigTotalItems = 0;
        this.bigCurrentPage = 1;
        this.numPages = 5;
        this.itemsPerPage = 10;
        this.bigCurrenItems = 1;
        this.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.datas = {
            'value': '',
            'per_page': this.itemsPerPage,
            'offset': this.offset
        };
    }
    FrontendComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loading = true;
        this.form();
        this.cms.listByPost(this.datas, this.formElem).then(function (response) {
            if (_this.formElem == 'service') {
                _this.service = response[0].data;
                _this.bigTotalItems = response[0].total;
                _this.loading = false;
            }
            else if (_this.formElem == 'skill') {
                _this.skill = response[0].data;
                _this.bigTotalItems = response[0].total;
                _this.loading = false;
            }
            else if (_this.formElem == 'history') {
                _this.history = response[0].data;
                _this.bigTotalItems = response[0].total;
                _this.loading = false;
            }
            else if (_this.formElem == 'home') {
                _this.loading = false;
                if (response[0]) {
                    _this.action = 'update';
                    _this.id = response[0].id;
                    _this.home = response[0];
                    _this.homeForm.patchValue({
                        heading: response[0].heading,
                        heading_description: response[0].heading_description,
                        subscribe_description: response[0].subscribe_description,
                        heading_background_color: response[0].heading_background_color,
                        services_background_color: response[0].services_background_color,
                        package_background_color: response[0].package_background_color,
                    });
                }
            }
            else if (_this.formElem == 'about') {
                _this.loading = false;
                if (response[0]) {
                    _this.action = 'update';
                    _this.id = response[0].id;
                    _this.about = response[0];
                    _this.aboutForm.patchValue({
                        title: response[0].title,
                        description: response[0].description,
                    });
                }
            }
            else if (_this.formElem == 'blog') {
                _this.loading = false;
                if (response[0]) {
                    _this.action = 'update';
                    _this.id = response[0].id;
                    _this.blog = response[0];
                    _this.blogForm.patchValue({
                        title: response[0].title,
                        description: response[0].description,
                    });
                }
            }
            else if (_this.formElem == 'portfolio') {
                _this.loading = false;
                if (response[0]) {
                    _this.action = 'update';
                    _this.id = response[0].id;
                    _this.portfolio = response[0];
                    _this.workForm.patchValue({
                        title: response[0].title,
                        description: response[0].description,
                    });
                }
            }
            else if (_this.formElem == 'contact') {
                _this.loading = false;
                if (response[0]) {
                    _this.action = 'update';
                    _this.id = response[0].id;
                    _this.contact = response[0];
                    _this.contactForm.patchValue({
                        title: response[0].title,
                        description: response[0].description,
                    });
                }
            }
        });
    };
    FrontendComponent.prototype.create = function (form, type, action, id) {
        if (id === void 0) { id = null; }
        var act = (type == 'on') ? true : false;
        this.show = act;
        this.formElem = form;
        this.action = action;
        this.id = id;
        if (action == 'edit') {
            this.edit(form, id);
        }
    };
    FrontendComponent.prototype.cancel = function (form, t) {
        this.ngOnInit();
        this.show = false;
    };
    FrontendComponent.prototype.save = function (action, form) {
        var _this = this;
        this.loading = true;
        var data = new FormData();
        if (form == 'home') {
            this.homeForm.value.header_image = this.image_f1;
            this.homeForm.value.package_image = this.image_f2;
            //this.homeForm.value.subscribe_image = this.image_f3;
            data.append('page', 'home');
            data.append('header_image', this.image_f1 == undefined ? '' : this.image_f1);
            data.append('package_image', this.image_f2 == undefined ? '' : this.image_f2);
            //data.append('subscribe_image', this.image_f3 == undefined ? '' : this.image_f3);
            data.append('heading', this.homeForm.value.heading == null ? '' : this.homeForm.value.heading);
            data.append('heading_description', this.homeForm.value.heading_description == null ? '' : this.homeForm.value.heading_description);
            data.append('subscribe_description', this.homeForm.value.subscribe_description == null ? '' : this.homeForm.value.subscribe_description);
            data.append('heading_background_color', this.homeForm.value.heading_background_color == null ? '' : this.homeForm.value.heading_background_color);
            data.append('package_background_color', this.homeForm.value.package_background_color == null ? '' : this.homeForm.value.package_background_color);
            data.append('services_background_color', this.homeForm.value.services_background_color == null ? '' : this.homeForm.value.services_background_color);
        }
        else if (form == 'contact') {
            data.append('page', 'contact');
            this.contactForm.value.image = this.image_f1;
            data.append('title', this.contactForm.value.title == null ? '' : this.contactForm.value.title);
            data.append('description', this.contactForm.value.description == null ? '' : this.contactForm.value.description);
            data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
        }
        else if (form == 'portfolio') {
            data.append('page', 'portfolio');
            this.workForm.value.image = this.image_f1;
            data.append('title', this.workForm.value.title == null ? '' : this.workForm.value.title);
            data.append('description', this.workForm.value.description == null ? '' : this.workForm.value.description);
            data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
        }
        else if (form == 'blog') {
            data.append('page', 'blog');
            this.blogForm.value.image = this.image_f1;
            data.append('title', this.blogForm.value.title == null ? '' : this.blogForm.value.title);
            data.append('description', this.blogForm.value.description == null ? '' : this.blogForm.value.description);
            data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
        }
        else if (form == 'about') {
            data.append('page', 'about');
            this.aboutForm.value.image = this.image_f1;
            data.append('title', this.aboutForm.value.title == null ? '' : this.aboutForm.value.title);
            data.append('description', this.aboutForm.value.description == null ? '' : this.aboutForm.value.description);
            data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
        }
        else if (form == 'history') {
            data.append('page', 'history');
            data.append('title', this.historyForm.value.title == null ? '' : this.historyForm.value.title);
            data.append('description', this.historyForm.value.description == null ? '' : this.historyForm.value.description);
            data.append('date', this.historyForm.value.date == null ? '' : this.historyForm.value.date);
        }
        else if (form == 'skill') {
            data.append('page', 'skill');
            this.skillForm.value.image = this.image_f1;
            data.append('title', this.skillForm.value.title == null ? '' : this.skillForm.value.title);
            data.append('description', this.skillForm.value.description == null ? '' : this.skillForm.value.description);
            data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
        }
        else if (form == 'service') {
            data.append('page', 'service');
            this.serviceForm.value.image = this.image_f1;
            data.append('title', this.serviceForm.value.title == null ? '' : this.serviceForm.value.title);
            data.append('description', this.serviceForm.value.description == null ? '' : this.serviceForm.value.description);
            data.append('image', this.image_f1 == undefined ? '' : this.image_f1);
        }
        if (action == 'update' && this.id !== '') {
            this.loading = true;
            this.cms.update(data, this.id).then(function (response) {
                _this.ngOnInit();
                _this.show = false;
                _this.loading = false;
                _this.notify.alert(response.success, 'success');
            }, function (error) {
                _this.notify.alert('Unable to store data.', 'error');
                _this.errors = JSON.parse(error._body);
            });
        }
        else {
            this.cms.store(data).then(function (response) {
                if (_this.formElem == 'skill' || _this.formElem == 'history' || _this.formElem == 'service') {
                    _this.ngOnInit();
                    _this.show = false;
                }
                else {
                    if (_this.active == 'home') {
                        _this.id = response.data.id;
                        _this.action = 'update';
                        _this.home = response.data;
                    }
                    else if (_this.active == 'portfolio') {
                        _this.id = response.data.id;
                        _this.action = 'update';
                        _this.portfolio = response.data;
                    }
                    else if (_this.active == 'about') {
                        _this.id = response.data.id;
                        _this.action = 'update';
                        _this.about = response.data;
                    }
                    else if (_this.active == 'blog') {
                        _this.id = response.data.id;
                        _this.action = 'update';
                        _this.blog = response.data;
                    }
                    else if (_this.active == 'contact') {
                        _this.id = response.data.id;
                        _this.action = 'update';
                        _this.contact = response.data;
                    }
                }
                _this.notify.alert(response.success, 'success');
                _this.loading = false;
            }, function (error) {
                _this.notify.alert('Unable to store data.', 'error');
                _this.errors = JSON.parse(error._body);
            });
        }
    };
    FrontendComponent.prototype.edit = function (form, id) {
        var _this = this;
        this.id = id;
        this.action = 'update';
        this.cms.get(id).then(function (response) {
            if (form == 'service') {
                _this.service = response[0];
                _this.serviceForm.patchValue({
                    title: response[0].title,
                    description: response[0].description,
                });
            }
            if (form == 'skill') {
                _this.skill = response[0];
                _this.skillForm.patchValue({
                    title: response[0].title,
                    description: response[0].description,
                });
            }
            if (form == 'history') {
                _this.history = response[0];
                _this.historyForm.patchValue({
                    title: response[0].title,
                    date: response[0].date,
                    description: response[0].description,
                });
            }
        });
    };
    FrontendComponent.prototype.deleteModal = function (name, id, template) {
        this.name = name;
        this.id = id;
        this.modalRef = this.modalService.show(template);
    };
    FrontendComponent.prototype.delete = function (form, id) {
        var _this = this;
        this.loading = true;
        this.cms.delete(id).then(function (response) {
            if (form == 'service') {
                _this.service = response;
            }
            else if (form == 'skill') {
                _this.skill = response;
            }
            else if (form == 'history') {
                _this.history = response;
            }
            _this.ngOnInit();
            _this.modalRef.hide();
            _this.notify.alert(response.success, 'success');
        }, function (error) {
            _this.notify.alert('Unable to delete item.', 'error');
            _this.errors = JSON.parse(error._body);
        });
    };
    FrontendComponent.prototype.getFile = function (event, input) {
        var file = event.target.files[0];
        if (input == 'image_f1')
            this.image_f1 = file;
        if (input == 'image_f2')
            this.image_f2 = file;
        if (input == 'image_f3')
            this.image_f3 = file;
    };
    FrontendComponent.prototype.form = function () {
        this.homeForm = new forms_1.FormGroup({
            heading: new forms_1.FormControl(),
            heading_description: new forms_1.FormControl(),
            subscribe_description: new forms_1.FormControl(),
            header_image: new forms_1.FormControl(),
            package_image: new forms_1.FormControl(),
            //subscribe_image: new FormControl(),
            heading_background_color: new forms_1.FormControl(),
            services_background_color: new forms_1.FormControl(),
            package_background_color: new forms_1.FormControl(),
        });
        this.serviceForm = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            image: new forms_1.FormControl(),
            description: new forms_1.FormControl('', [forms_1.Validators.required]),
        });
        this.blogForm = new forms_1.FormGroup({
            image: new forms_1.FormControl(),
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required])
        });
        this.workForm = new forms_1.FormGroup({
            image: new forms_1.FormControl(),
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required])
        });
        this.contactForm = new forms_1.FormGroup({
            image: new forms_1.FormControl(),
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required])
        });
        this.aboutForm = new forms_1.FormGroup({
            image: new forms_1.FormControl(),
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required])
        });
        this.skillForm = new forms_1.FormGroup({
            image: new forms_1.FormControl(),
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required])
        });
        this.historyForm = new forms_1.FormGroup({
            date: new forms_1.FormControl('', [forms_1.Validators.required]),
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required])
        });
    };
    FrontendComponent.prototype.setElemForm = function (elem) {
        this.formElem = elem;
        this.errors = new shared_1.Errors;
        this.show = false;
        this.datas.value = '';
        this.ngOnInit();
    };
    FrontendComponent.prototype.search = function (value) {
        this.datas.value = value;
        this.ngOnInit();
    };
    FrontendComponent.prototype.refresh = function () {
        this.ngOnInit();
    };
    FrontendComponent.prototype.perPage = function (value) {
        this.itemsPerPage = value;
        this.ngOnInit();
    };
    FrontendComponent.prototype.pageChanged = function (event) {
        this.bigCurrentPage = event.page;
        this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.ngOnInit();
    };
    FrontendComponent.prototype.setActive = function (tab) {
        this.errors = new shared_1.Errors;
        this.active = tab;
        this.formElem = tab;
        this.show = false;
        this.datas.value = '';
        this.id = '';
        this.action = 'post';
        if (tab == 'home' || tab == 'portfolio' || tab == 'blog' || tab == 'contact' || tab == 'about') {
            this.ngOnInit();
        }
    };
    FrontendComponent = __decorate([
        core_1.Component({
            selector: 'app-frontend',
            templateUrl: './frontend.component.html',
        }),
        __metadata("design:paramtypes", [_1.BsModalService,
            shared_1.CmsService,
            shared_1.AlertService])
    ], FrontendComponent);
    return FrontendComponent;
}());
exports.FrontendComponent = FrontendComponent;
//# sourceMappingURL=frontend.component.js.map