"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var services_1 = require("../shared/services");
var BlogDetailComponent = /** @class */ (function () {
    function BlogDetailComponent(blog, route, router, seo) {
        this.blog = blog;
        this.route = route;
        this.router = router;
        this.seo = seo;
        this.slug = this.route.snapshot.params['slug'];
        this.pageId = '/blog/' + this.slug;
        this.loading_articles = true;
        this.loading_categories = true;
    }
    BlogDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.blog.getBySlug(this.slug).then(function (response) {
            _this.post = response[0];
            _this.seo.setTitle('Ryan Dingle - ' + _this.post.title);
            _this.loading_articles = false;
            /*SEO META UPDATE*/
            _this.seo.updateKeyword(_this.post.tags);
            _this.seo.updateTitle(_this.post.title);
            _this.seo.updateDescription(response[0].body);
            _this.seo.updateSummary(response[0].body);
            _this.seo.updateImage(_this.post.image);
        });
        this.blog.getRecentPost().then(function (response) {
            _this.recentPost = response;
            _this.loading_categories = false;
        });
    };
    BlogDetailComponent.prototype.navigate = function (slug) {
        var _this = this;
        this.loading_articles = true;
        this.loading_categories = true;
        this.route.params.subscribe(function (res) {
            _this.router.navigate(['blog', slug]).then(function (res) {
                _this.pageId = '/works/' + slug;
                _this.blog.getBySlug(slug).then(function (response) {
                    _this.post = response[0];
                    _this.loading_articles = false;
                    /*SEO META UPDATE*/
                    _this.seo.updateKeyword(_this.post.tags);
                    _this.seo.updateTitle(_this.post.title);
                    _this.seo.updateDescription(response[0].body);
                    _this.seo.updateSummary(response[0].body);
                    _this.seo.updateImage(_this.post.image);
                });
                _this.blog.getRecentPost().then(function (response) {
                    _this.recentPost = response;
                    _this.loading_categories = false;
                });
            });
        });
    };
    BlogDetailComponent = __decorate([
        core_1.Component({
            selector: 'app-blog-detail',
            templateUrl: './blog-detail.component.html'
        }),
        __metadata("design:paramtypes", [services_1.BlogService,
            router_1.ActivatedRoute,
            router_1.Router,
            services_1.SeoService])
    ], BlogDetailComponent);
    return BlogDetailComponent;
}());
exports.BlogDetailComponent = BlogDetailComponent;
//# sourceMappingURL=blog-detail.component.js.map