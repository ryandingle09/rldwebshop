"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var blog_component_1 = require("./blog.component");
var blog_detail_component_1 = require("./blog-detail.component");
var blog_category_component_1 = require("./blog-category.component");
var ng2_awesome_disqus_1 = require("ng2-awesome-disqus");
var ngx_sharebuttons_1 = require("ngx-sharebuttons");
var shared_1 = require("../shared");
var Routing = router_1.RouterModule.forChild([
    {
        path: 'blog',
        component: blog_component_1.BlogComponent,
        data: {
            page: 'blog',
            title: 'RLD Webshop - Blog'
        }
    },
    {
        path: 'blog/:slug',
        component: blog_detail_component_1.BlogDetailComponent,
        data: {
            page: 'blogdetail',
            title: 'RLD Webshop - Blog Details'
        }
    },
    {
        path: 'blog/cat/:id',
        component: blog_category_component_1.BlogCategoryComponent,
        data: {
            page: 'blog',
            title: 'RLD Webshop - Blog Category'
        }
    }
]);
var BlogModule = /** @class */ (function () {
    function BlogModule() {
    }
    BlogModule = __decorate([
        core_1.NgModule({
            imports: [
                Routing,
                shared_1.SharedModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ngx_bootstrap_1.PaginationModule.forRoot(),
                ng2_awesome_disqus_1.DisqusModule,
                ngx_sharebuttons_1.ShareButtonsModule.forRoot(),
                animations_1.NoopAnimationsModule
            ],
            declarations: [
                blog_component_1.BlogComponent,
                blog_detail_component_1.BlogDetailComponent,
                blog_category_component_1.BlogCategoryComponent
            ],
            providers: [shared_1.CmsService]
        })
    ], BlogModule);
    return BlogModule;
}());
exports.BlogModule = BlogModule;
//# sourceMappingURL=blog.module.js.map