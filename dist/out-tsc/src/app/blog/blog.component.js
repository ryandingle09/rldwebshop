"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var shared_1 = require("../shared");
var BlogComponent = /** @class */ (function () {
    function BlogComponent(blog, router, seo, storage) {
        this.blog = blog;
        this.router = router;
        this.seo = seo;
        this.storage = storage;
        this.maxSize = 5;
        this.bigTotalItems = 0;
        this.bigCurrentPage = 1;
        this.numPages = 5;
        this.itemsPerPage = 10;
        this.bigCurrenItems = 1;
        this.post = [];
        this.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.datas = {
            'value': '',
            'per_page': this.itemsPerPage,
            'offset': this.offset
        };
        this.loading_articles = true;
        this.loading_categories = true;
        this.image = '/assets/images/headers/index2.jpg';
    }
    BlogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.blog_data = this.storage.get('blog');
        this.image = this.blog_data[0].image == null ? this.image : this.blog_data[0].image;
        /*SEO META UPDATE*/
        this.seo.updateKeyword('RLD Webshop, Blog Page, Blog');
        this.seo.updateTitle('RLD Webshop - Blog');
        this.seo.updateDescription('RLD Webshop Blog Website');
        this.seo.updateAuthor('Ryan L. Dingle');
        this.seo.updateImage(this.image);
        this.blog.list(this.datas).then(function (response) {
            _this.post = response[0].data;
            _this.bigTotalItems = response[0].total;
            _this.loading_articles = false;
        });
        this.blog.getCategoryList().then(function (response) {
            _this.categories = response;
            _this.loading_categories = false;
        });
    };
    BlogComponent.prototype.perPage = function (value) {
        this.itemsPerPage = value;
        this.ngOnInit();
    };
    BlogComponent.prototype.pageChanged = function (event) {
        this.bigCurrentPage = event.page;
        this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.ngOnInit();
    };
    BlogComponent = __decorate([
        core_1.Component({
            selector: 'app-blog',
            templateUrl: './blog.component.html',
        }),
        __metadata("design:paramtypes", [shared_1.BlogService,
            router_1.Router,
            shared_1.SeoService,
            shared_1.StorageService])
    ], BlogComponent);
    return BlogComponent;
}());
exports.BlogComponent = BlogComponent;
//# sourceMappingURL=blog.component.js.map