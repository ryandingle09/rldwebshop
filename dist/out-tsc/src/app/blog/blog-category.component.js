"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var services_1 = require("../shared/services");
var BlogCategoryComponent = /** @class */ (function () {
    function BlogCategoryComponent(blog, router, route, seo) {
        this.blog = blog;
        this.router = router;
        this.route = route;
        this.seo = seo;
        this.maxSize = 5;
        this.bigTotalItems = 0;
        this.bigCurrentPage = 1;
        this.numPages = 5;
        this.itemsPerPage = 10;
        this.bigCurrenItems = 1;
        this.post = [];
        this.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.id = this.route.snapshot.params['id'];
        this.datas = {
            'value': '',
            'per_page': this.itemsPerPage,
            'offset': this.offset,
            'id': this.id
        };
        this.loading_articles = true;
        this.loading_categories = true;
    }
    BlogCategoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.blog.listByCategory(this.datas, this.id).then(function (response) {
            _this.post = response[0].data;
            _this.bigTotalItems = response[0].total;
            _this.loading_articles = false;
            /*SEO META UPDATE*/
            _this.seo.updateKeyword(_this.post[0].tags);
            _this.seo.updateTitle(_this.post[0].title);
            _this.seo.updateDescription(_this.post[0].description);
            _this.seo.updateSummary(_this.post[0].description);
            _this.seo.updateImage(_this.post[0].image);
        });
        this.blog.getCategoryList().then(function (response) {
            _this.categories = response;
            _this.loading_categories = false;
        });
    };
    BlogCategoryComponent.prototype.navigate = function (id) {
        var _this = this;
        this.loading_articles = true;
        this.datas = {
            'value': '',
            'per_page': this.itemsPerPage,
            'offset': this.offset,
            'id': id
        };
        this.route.params.subscribe(function (res) {
            _this.router.navigate(['blog/cat/', id]).then(function (res) {
                _this.blog.listByCategory(_this.datas, id).then(function (response) {
                    _this.post = response[0].data;
                    _this.bigTotalItems = response[0].total;
                    _this.loading_articles = false;
                });
            });
        });
    };
    BlogCategoryComponent.prototype.perPage = function (value) {
        this.itemsPerPage = value;
        this.ngOnInit();
    };
    BlogCategoryComponent.prototype.pageChanged = function (event) {
        this.bigCurrentPage = event.page;
        this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.ngOnInit();
    };
    BlogCategoryComponent = __decorate([
        core_1.Component({
            selector: 'app-blog-category',
            templateUrl: './blog-category.component.html'
        }),
        __metadata("design:paramtypes", [services_1.BlogService,
            router_1.Router,
            router_1.ActivatedRoute,
            services_1.SeoService])
    ], BlogCategoryComponent);
    return BlogCategoryComponent;
}());
exports.BlogCategoryComponent = BlogCategoryComponent;
//# sourceMappingURL=blog-category.component.js.map