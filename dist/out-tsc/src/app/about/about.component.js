"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var shared_1 = require("../shared");
var AboutComponent = /** @class */ (function () {
    function AboutComponent(seo, storage) {
        this.seo = seo;
        this.storage = storage;
        this.image = '/assets/images/headers/index2.jpg';
    }
    AboutComponent.prototype.ngOnInit = function () {
        this.about = this.storage.get('about');
        this.skill = this.storage.get('skill');
        this.history = this.storage.get('history');
        this.image = this.about[0].image == null ? this.image : this.about[0].image;
        /*SEO META UPDATE*/
        this.seo.update({ name: 'keywords', content: 'RLD Webshop, About' });
        this.seo.updateTitle('RLD Webshop - About');
        this.seo.updateDescription('RLD Webshop About Page');
        this.seo.updateImage(this.image);
    };
    AboutComponent = __decorate([
        core_1.Component({
            selector: 'app-about',
            templateUrl: './about.component.html',
            styleUrls: ['./about.component.css']
        }),
        __metadata("design:paramtypes", [shared_1.SeoService,
            shared_1.StorageService])
    ], AboutComponent);
    return AboutComponent;
}());
exports.AboutComponent = AboutComponent;
//# sourceMappingURL=about.component.js.map