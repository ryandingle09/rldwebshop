"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var services_1 = require("../shared/services");
var AccountComponent = /** @class */ (function () {
    function AccountComponent(auth, userInfo, notify) {
        this.auth = auth;
        this.userInfo = userInfo;
        this.notify = notify;
        this.showForm = 0;
        this.id = '';
        this.userCookie = this.auth.getUserToken();
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.createForm();
    };
    AccountComponent.prototype.hide = function () {
        this.showForm = 0;
    };
    AccountComponent.prototype.show = function () {
        this.showForm = 1;
        this.form.patchValue({
            firstname: this.userCookie['firstname'],
            email: this.userCookie['email'],
            lastname: this.userCookie['lastname'],
        });
    };
    AccountComponent.prototype.getFile = function (event) {
        var file = event.target.files[0];
        this.photo = file;
    };
    AccountComponent.prototype.createForm = function () {
        this.form = new forms_1.FormGroup({
            image: new forms_1.FormControl('', [forms_1.Validators.required]),
            firstname: new forms_1.FormControl('', [forms_1.Validators.required]),
            email: new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.email]),
            lastname: new forms_1.FormControl('', [forms_1.Validators.required]),
            password: new forms_1.FormControl('', [forms_1.Validators.required]),
            password2: new forms_1.FormControl('', [forms_1.Validators.required]),
        }, this.passwordMatchValidator);
    };
    AccountComponent.prototype.passwordMatchValidator = function (g) {
        return g.get('password').value === g.get('password2').value ? null : { 'mismatch': true };
    };
    AccountComponent.prototype.save = function () {
        var _this = this;
        var data = new FormData();
        data.append('email', this.form.value.email == null ? '' : this.form.value.email);
        data.append('firstname', this.form.value.firstname == null ? '' : this.form.value.firstname);
        data.append('lastname', this.form.value.lastname == null ? '' : this.form.value.lastname);
        data.append('password', this.form.value.password == null ? '' : this.form.value.password);
        data.append('password_confirmation', this.form.value.password2 == null ? '' : this.form.value.password2);
        data.append('photo', this.photo == undefined ? '' : this.photo);
        var id = this.userCookie['id'];
        this.userInfo.update(id, data).then(function (response) {
            _this.UserData = response.user;
            _this.auth.updateUserToken(response.user);
            _this.userCookie = _this.auth.getUserToken();
            _this.notify.alert('Account Successfully Updated.', 'success');
        }, function (error) {
            _this.errors = JSON.parse(error._body);
            _this.notify.alert('Server validation Error.', 'danger');
        });
    };
    AccountComponent = __decorate([
        core_1.Component({
            selector: 'app-account',
            templateUrl: './account.component.html',
        }),
        __metadata("design:paramtypes", [services_1.AuthService,
            services_1.UserService,
            services_1.AlertService])
    ], AccountComponent);
    return AccountComponent;
}());
exports.AccountComponent = AccountComponent;
//# sourceMappingURL=account.component.js.map