"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var animations_1 = require("@angular/platform-browser/animations");
var forms_1 = require("@angular/forms");
var account_component_1 = require("./account.component");
var shared_1 = require("../shared");
var angular2_notifications_1 = require("angular2-notifications");
var Routing = router_1.RouterModule.forChild([
    {
        path: 'account',
        component: account_component_1.AccountComponent,
        canActivate: [shared_1.AuthGuard],
        data: {
            page: 'account',
            title: 'RLD Webshop - My Account'
        }
    }
]);
var AccountModule = /** @class */ (function () {
    function AccountModule() {
    }
    AccountModule = __decorate([
        core_1.NgModule({
            imports: [
                Routing,
                shared_1.SharedModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                angular2_notifications_1.SimpleNotificationsModule.forRoot(),
                animations_1.NoopAnimationsModule
            ],
            declarations: [
                account_component_1.AccountComponent
            ],
            providers: [
                shared_1.AuthGuard,
                shared_1.AuthService,
                shared_1.CmsService,
                shared_1.UserService
            ]
        })
    ], AccountModule);
    return AccountModule;
}());
exports.AccountModule = AccountModule;
//# sourceMappingURL=account.module.js.map