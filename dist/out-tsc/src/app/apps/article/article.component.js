"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var modal_1 = require("ngx-bootstrap/modal");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var shared_1 = require("../../shared");
var models_1 = require("../../shared/models");
var services_1 = require("../../shared/services");
var ArticleComponent = /** @class */ (function () {
    function ArticleComponent(modalService, blog, portfolio, tag, category, auth, router, notify, platformId) {
        this.modalService = modalService;
        this.blog = blog;
        this.portfolio = portfolio;
        this.tag = tag;
        this.category = category;
        this.auth = auth;
        this.router = router;
        this.notify = notify;
        this.platformId = platformId;
        this.config = shared_1.SelectMultiConfig;
        this.placeholder1 = 'Add Category';
        this.placeholder2 = 'Add Tag';
        this.active = 'blog';
        this.showForm = 0;
        this.maxSize = 5;
        this.bigTotalItems = 0;
        this.bigCurrentPage = 1;
        this.numPages = 5;
        this.itemsPerPage = 10;
        this.bigCurrenItems = 1;
        this.project = [];
        this.post = [];
        this.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.datas = {
            'value': '',
            'per_page': this.itemsPerPage,
            'offset': this.offset
        };
        this.isBrowser = common_1.isPlatformBrowser(platformId);
    }
    ArticleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm2();
        this.createForm3();
        if (this.active == 'blog') {
            this.blog.list(this.datas).then(function (response) {
                _this.post = response[0].data;
                _this.bigTotalItems = response[0].total;
            });
        }
        else if (this.active == 'category') {
            this.category.list2(this.datas).then(function (response) {
                _this.categories = response[0].data;
                _this.bigTotalItems = response[0].total;
            });
        }
        else {
            this.tag.list2(this.datas).then(function (response) {
                _this.tags = response[0].data;
                _this.bigTotalItems = response[0].total;
            });
        }
    };
    ArticleComponent.prototype.create = function (action, title, id) {
        var _this = this;
        this.fValue = undefined;
        this.name = title;
        this.id = id;
        this.action = action;
        if (action == 'edit' && title == 'Blog') {
            this.blog.edit(id).then(function (response) {
                var data = response;
                _this.value1 = response.categories;
                _this.value2 = response.tags;
                _this.form3.patchValue({
                    title: data.data.title,
                    slug: data.data.slug,
                    body: data.data.body,
                    category: response.categories,
                    tags: response.tags,
                    status: data.data.status,
                });
            });
        }
        else if (action == 'edit' && title == 'Category') {
            this.category.edit(id).then(function (response) {
                var data = response[0];
                _this.form2.patchValue({
                    title: data.title,
                    description: data.description,
                    status: data.status,
                });
            });
        }
        else if (action == 'edit' && title == 'Tag') {
            this.tag.edit(id).then(function (response) {
                var data = response[0];
                _this.form2.patchValue({
                    title: data.title,
                    description: data.description,
                    status: data.status,
                });
            });
        }
        else {
            this.data = {};
            this.value1 = [];
            this.value2 = [];
        }
        this.showForm = 1;
    };
    ArticleComponent.prototype.hide = function () {
        this.showForm = 0;
        this.form3.patchValue({
            title: '',
            slug: '',
            body: '',
        });
        this.form2.patchValue({
            title: '',
            description: '',
        });
    };
    ArticleComponent.prototype.search = function (value) {
        this.datas.value = value;
        this.ngOnInit();
    };
    ArticleComponent.prototype.refresh = function () {
        this.ngOnInit();
    };
    ArticleComponent.prototype.perPage = function (value) {
        this.itemsPerPage = value;
        this.ngOnInit();
    };
    ArticleComponent.prototype.pageChanged = function (event) {
        this.bigCurrentPage = event.page;
        this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.ngOnInit();
    };
    ArticleComponent.prototype.setActive = function (tab) {
        this.errors = new models_1.Errors;
        this.active = tab;
        this.showForm = false;
        this.datas.value = '';
        this.ngOnInit();
    };
    ArticleComponent.prototype.deleteModal = function (name, id, template) {
        this.name = name;
        this.id = id;
        if (this.isBrowser) {
            this.modalRef = this.modalService.show(template);
        }
    };
    ArticleComponent.prototype.delete = function (name, id) {
        var _this = this;
        if (name == 'Blog') {
            this.blog.delete(id).then(function (response) {
                _this.ngOnInit();
                if (_this.isBrowser) {
                    _this.modalRef.hide();
                    _this.notify.alert(response.success, 'info');
                }
            }, function (error) {
                _this.errors = JSON.parse(error._body);
            });
        }
        else if (name == 'Category') {
            this.category.delete(id).then(function (response) {
                _this.ngOnInit();
                if (_this.isBrowser) {
                    _this.modalRef.hide();
                    _this.notify.alert(response.success, 'info');
                }
            }, function (error) {
                _this.errors = JSON.parse(error._body);
            });
        }
        else {
            this.tag.delete(id).then(function (response) {
                _this.ngOnInit();
                if (_this.isBrowser) {
                    _this.modalRef.hide();
                    _this.notify.alert(response.success, 'info');
                }
            }, function (error) {
                _this.errors = JSON.parse(error._body);
            });
        }
    };
    ArticleComponent.prototype.createForm2 = function () {
        this.form2 = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required]),
            status: new forms_1.FormControl('published', [forms_1.Validators.required]),
        });
    };
    ArticleComponent.prototype.createForm3 = function () {
        var _this = this;
        this.category.list().then(function (response) { return _this.categories = response; });
        this.tag.list().then(function (response) { return _this.tags = response; });
        this.form3 = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            slug: new forms_1.FormControl('', [forms_1.Validators.required]),
            body: new forms_1.FormControl('', [forms_1.Validators.required]),
            image: new forms_1.FormControl(),
            tag: new forms_1.FormControl('', [forms_1.Validators.required]),
            category: new forms_1.FormControl('', [forms_1.Validators.required]),
            status: new forms_1.FormControl('published', [forms_1.Validators.required])
        });
    };
    ArticleComponent.prototype.getFile = function (event) {
        var file = event.target.files[0];
        this.fValue = file;
    };
    ArticleComponent.prototype.save = function (type) {
        var _this = this;
        this.form3.value.realImage = this.fValue;
        var data = new FormData();
        data.append('title', this.form3.value.title == null ? '' : this.form3.value.title);
        data.append('slug', this.form3.value.slug == null ? '' : this.form3.value.slug);
        data.append('body', this.form3.value.body == null ? '' : this.form3.value.body);
        data.append('category', this.form3.value.category == null ? '' : this.form3.value.category);
        data.append('tag', this.form3.value.tag == null ? '' : this.form3.value.tag);
        data.append('status', this.form3.value.status == null ? '' : this.form3.value.status);
        data.append('photo', this.fValue !== undefined ? this.fValue : '');
        if (this.name == 'Blog') {
            if (type == 'post') { //post process
                this.blog.store(data).then(function (response) {
                    _this.ngOnInit();
                    _this.errors = response;
                    _this.value1 = [];
                    _this.value2 = [];
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) {
                    _this.errors = JSON.parse(error._body);
                });
            }
            else { //update process
                this.blog.update(data, this.id).then(function (response) {
                    _this.ngOnInit();
                    _this.errors = response;
                    _this.showForm = 0;
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) {
                    _this.errors = JSON.parse(error._body);
                });
            }
        }
        else if (this.name == 'Category') {
            if (type == 'post') {
                this.category.store(this.form2.value).then(function (response) {
                    _this.ngOnInit();
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) { return _this.errors = JSON.parse(error._body); });
            }
            else {
                this.category.update(this.form2.value, this.id).then(function (response) {
                    _this.ngOnInit();
                    _this.showForm = 0;
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) { return _this.errors = JSON.parse(error._body); });
            }
        }
        else {
            if (type == 'post') {
                this.tag.store(this.form2.value).then(function (response) {
                    _this.ngOnInit();
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) { return _this.errors = JSON.parse(error._body); });
            }
            else {
                this.tag.update(this.form2.value, this.id).then(function (response) {
                    _this.ngOnInit();
                    _this.showForm = 0;
                    if (_this.isBrowser) {
                        _this.notify.alert(response.success, 'success');
                    }
                }, function (error) { return _this.errors = JSON.parse(error._body); });
            }
        }
    };
    ArticleComponent.prototype.slug = function (slug) {
        var word = slug.split(" ").join("-").toLowerCase();
        word = word.split("/").join("-").toLowerCase();
        this.form3.patchValue({ slug: word });
    };
    ArticleComponent = __decorate([
        core_1.Component({
            selector: 'app-article',
            templateUrl: './article.component.html',
        }),
        __param(8, core_1.Inject(core_1.PLATFORM_ID)),
        __metadata("design:paramtypes", [modal_1.BsModalService,
            services_1.BlogService,
            services_1.PortfolioService,
            services_1.TagService,
            services_1.CategoryService,
            services_1.AuthService,
            router_1.Router,
            services_1.AlertService,
            Object])
    ], ArticleComponent);
    return ArticleComponent;
}());
exports.ArticleComponent = ArticleComponent;
//# sourceMappingURL=article.component.js.map