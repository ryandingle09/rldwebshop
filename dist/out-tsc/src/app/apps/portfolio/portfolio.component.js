"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var modal_1 = require("ngx-bootstrap/modal");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var shared_1 = require("../../shared");
var models_1 = require("../../shared/models");
var services_1 = require("../../shared/services");
var PortfolioComponent = /** @class */ (function () {
    function PortfolioComponent(modalService, blog, portfolio, tag, category, auth, router, notify, platformId) {
        this.modalService = modalService;
        this.blog = blog;
        this.portfolio = portfolio;
        this.tag = tag;
        this.category = category;
        this.auth = auth;
        this.router = router;
        this.notify = notify;
        this.platformId = platformId;
        this.config = shared_1.SelectMultiConfig;
        this.placeholder1 = 'Add Category';
        this.placeholder2 = 'Add Tag';
        this.active = 'portfolio';
        this.showForm = 0;
        this.maxSize = 5;
        this.bigTotalItems = 0;
        this.bigCurrentPage = 1;
        this.numPages = 5;
        this.itemsPerPage = 10;
        this.bigCurrenItems = 1;
        this.project = [];
        this.post = [];
        this.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.datas = {
            'value': '',
            'per_page': this.itemsPerPage,
            'offset': this.offset
        };
        this.isBrowser = common_1.isPlatformBrowser(platformId);
    }
    PortfolioComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createForm2();
        this.createForm();
        this.portfolio.list(this.datas).then(function (response) {
            _this.project = response[0].data;
            _this.bigTotalItems = response[0].total;
        });
    };
    PortfolioComponent.prototype.create = function (action, title, id) {
        var _this = this;
        this.fValue = undefined;
        this.name = title;
        this.id = id;
        this.action = action;
        if (action == 'edit') {
            this.portfolio.edit(id).then(function (response) {
                var data = response;
                _this.value1 = response.categories;
                _this.form.patchValue({
                    title: data.data.title,
                    slug: data.data.slug,
                    description: data.data.description,
                    category: response.categories,
                    link: data.data.link,
                    status: data.data.status,
                });
            });
        }
        else {
            this.data = {};
            this.value1 = [];
            this.value2 = [];
        }
        this.showForm = 1;
    };
    PortfolioComponent.prototype.hide = function () {
        this.showForm = 0;
    };
    PortfolioComponent.prototype.search = function (value) {
        this.datas.value = value;
        this.ngOnInit();
    };
    PortfolioComponent.prototype.refresh = function () {
        this.ngOnInit();
    };
    PortfolioComponent.prototype.perPage = function (value) {
        this.itemsPerPage = value;
        this.ngOnInit();
    };
    PortfolioComponent.prototype.pageChanged = function (event) {
        this.bigCurrentPage = event.page;
        this.datas.offset = (this.bigCurrentPage == 1) ? 0 : this.bigCurrentPage * this.itemsPerPage;
        this.ngOnInit();
    };
    PortfolioComponent.prototype.setActive = function (tab) {
        this.errors = new models_1.Errors;
        this.active = tab;
        this.showForm = false;
        this.datas.value = '';
        this.ngOnInit();
    };
    PortfolioComponent.prototype.deleteModal = function (name, id, template) {
        this.name = name;
        this.id = id;
        this.modalRef = this.modalService.show(template);
    };
    PortfolioComponent.prototype.delete = function (name, id) {
        var _this = this;
        this.portfolio.delete(id).then(function (response) {
            _this.ngOnInit();
            _this.modalRef.hide();
            _this.notify.alert(response.success, 'info');
        }, function (error) {
            _this.errors = JSON.parse(error._body);
        });
    };
    PortfolioComponent.prototype.openModal2 = function (name, template) {
        this.title2 = name;
        this.modalRef2 = this.modalService.show(template);
        this.createForm2();
    };
    PortfolioComponent.prototype.createForm2 = function () {
        this.form2 = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required]),
            status: new forms_1.FormControl('published', [forms_1.Validators.required]),
        });
    };
    PortfolioComponent.prototype.createForm = function () {
        var _this = this;
        this.category.list().then(function (response) { return _this.categories = response; });
        this.form = new forms_1.FormGroup({
            title: new forms_1.FormControl('', [forms_1.Validators.required]),
            slug: new forms_1.FormControl('', [forms_1.Validators.required]),
            description: new forms_1.FormControl('', [forms_1.Validators.required]),
            link: new forms_1.FormControl('', [forms_1.Validators.pattern("http?://.+|https?://.+")]),
            image: new forms_1.FormControl(),
            category: new forms_1.FormControl('', [forms_1.Validators.required]),
            status: new forms_1.FormControl('published', [forms_1.Validators.required])
        });
    };
    PortfolioComponent.prototype.getFile = function (event) {
        var file = event.target.files[0];
        this.fValue = file;
    };
    PortfolioComponent.prototype.save2 = function (name) {
        var _this = this;
        var data = this.form2.value;
        if (name == 'Category') {
            this.category.store(data).then(function (response) {
                _this.category.list().then(function (response) { return _this.categories = response; });
                _this.modalRef2.hide();
                _this.notify.alert(response.success, 'success');
            }, function (error) { return _this.errors = JSON.parse(error._body); });
        }
        else {
            this.tag.store(data).then(function (response) {
                _this.modalRef2.hide();
                _this.notify.alert(response.success, 'success');
                _this.tag.list().then(function (response) { return _this.tags = response; });
            }, function (error) { return _this.errors = JSON.parse(error._body); });
        }
    };
    PortfolioComponent.prototype.save = function (type) {
        var _this = this;
        this.form.value.realImage = this.fValue;
        var data = new FormData();
        data.append('title', this.form.value.title == null ? '' : this.form.value.title);
        data.append('slug', this.form.value.slug == null ? '' : this.form.value.slug);
        data.append('description', this.form.value.description == null ? '' : this.form.value.description);
        data.append('link', this.form.value.link == null ? '' : this.form.value.link);
        data.append('category', this.form.value.category == null ? '' : this.form.value.category);
        data.append('status', this.form.value.status == null ? '' : this.form.value.status);
        data.append('photo', this.fValue !== undefined ? this.fValue : '');
        if (type == 'post') { //post process
            this.portfolio.store(data).then(function (response) {
                _this.ngOnInit();
                _this.value1 = [];
                _this.category.list().then(function (response) { return _this.categories = response; });
                _this.notify.alert(response.success, 'success');
                _this.errors = new models_1.Errors;
            }, function (error) {
                _this.errors = JSON.parse(error._body);
            });
        }
        else { //update process
            this.portfolio.update(data, this.id).then(function (response) {
                _this.ngOnInit();
                _this.showForm = 0;
                _this.notify.alert(response.success, 'success');
                _this.errors = new models_1.Errors;
            }, function (error) {
                _this.errors = JSON.parse(error._body);
            });
        }
    };
    PortfolioComponent.prototype.slug = function (slug) {
        this.form.patchValue({ slug: slug.split(" ").join("-").toLowerCase() });
    };
    PortfolioComponent = __decorate([
        core_1.Component({
            selector: 'app-portfolio',
            templateUrl: './portfolio.component.html',
        }),
        __param(8, core_1.Inject(core_1.PLATFORM_ID)),
        __metadata("design:paramtypes", [modal_1.BsModalService,
            services_1.BlogService,
            services_1.PortfolioService,
            services_1.TagService,
            services_1.CategoryService,
            services_1.AuthService,
            router_1.Router,
            services_1.AlertService,
            Object])
    ], PortfolioComponent);
    return PortfolioComponent;
}());
exports.PortfolioComponent = PortfolioComponent;
//# sourceMappingURL=portfolio.component.js.map