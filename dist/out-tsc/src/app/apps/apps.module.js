"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var notification_component_1 = require("./notification/notification.component");
var article_component_1 = require("./article/article.component");
var portfolio_component_1 = require("./portfolio/portfolio.component");
var shared_1 = require("../shared");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var ng_selectize_1 = require("ng-selectize");
//import { NotifyModule, NotifyService } from 'notify-angular';
var angular2_notifications_1 = require("angular2-notifications");
var ng2_ckeditor_1 = require("ng2-ckeditor");
var Routing = router_1.RouterModule.forChild([
    {
        path: 'notification',
        component: notification_component_1.NotificationComponent,
        canActivate: [shared_1.AuthGuard],
        data: {
            page: 'notification',
            title: 'RLD Webshop - My Notifications'
        }
    },
    {
        path: 'article',
        component: article_component_1.ArticleComponent,
        canActivate: [shared_1.AuthGuard],
        data: {
            page: 'article',
            title: 'RLD Webshop - My Articles'
        }
    },
    {
        path: 'portfolio',
        component: portfolio_component_1.PortfolioComponent,
        canActivate: [shared_1.AuthGuard],
        data: {
            page: 'portfolio',
            title: 'RLD Webshop - My Portfolio'
        }
    },
]);
var AppsModule = /** @class */ (function () {
    function AppsModule() {
    }
    AppsModule = __decorate([
        core_1.NgModule({
            imports: [
                Routing,
                shared_1.SharedModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ng_selectize_1.NgSelectizeModule,
                ngx_bootstrap_1.PaginationModule.forRoot(),
                animations_1.NoopAnimationsModule,
                angular2_notifications_1.SimpleNotificationsModule.forRoot(),
                ng2_ckeditor_1.CKEditorModule
            ],
            declarations: [
                notification_component_1.NotificationComponent,
                portfolio_component_1.PortfolioComponent,
                article_component_1.ArticleComponent
            ],
            providers: [shared_1.AuthGuard]
        })
    ], AppsModule);
    return AppsModule;
}());
exports.AppsModule = AppsModule;
//# sourceMappingURL=apps.module.js.map