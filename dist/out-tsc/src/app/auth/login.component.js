"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var shared_1 = require("../shared");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(auth, router, seo) {
        this.auth = auth;
        this.router = router;
        this.seo = seo;
    }
    LoginComponent.prototype.ngOnInit = function () {
        /*SEO META UPDATE*/
        this.seo.updateKeyword('RLD Webshop, Login, Authentication');
        this.seo.updateTitle('RLD Webshop - Login');
        this.seo.updateDescription('RLD Webshop Login Page');
        this.seo.updateSummary('RLD Webshop Login Page');
        this.seo.updateImage('/assets/images/headers/index2.jpg');
        this.createForm();
    };
    LoginComponent.prototype.createForm = function () {
        this.form = new forms_1.FormGroup({
            email: new forms_1.FormControl(),
            password: new forms_1.FormControl()
        });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.auth.login(this.form.value)
            .then(function (response) {
            _this.users = response.data;
            _this.auth.setLogin(response.data, response.token);
            _this.router.navigateByUrl('/admin');
        }, function (err) {
            _this.errors = JSON.parse(err._body);
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
        }),
        __metadata("design:paramtypes", [shared_1.AuthService,
            router_1.Router,
            shared_1.SeoService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map