"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var animations_1 = require("@angular/platform-browser/animations");
var forms_1 = require("@angular/forms");
var login_component_1 = require("./login.component");
var register_component_1 = require("./register.component");
var shared_1 = require("../shared");
var services_1 = require("../shared/services");
var Routing = router_1.RouterModule.forChild([
    {
        path: 'auth/login',
        component: login_component_1.LoginComponent,
        canActivate: [services_1.NoAuthGuard],
        data: {
            page: 'login',
            title: 'Ryan Dingle - Login'
        }
    },
    {
        path: 'auth/register',
        component: register_component_1.RegisterComponent,
        canActivate: [services_1.NoAuthGuard],
        data: {
            page: 'register',
            title: 'Ryan Dingle - Register'
        }
    }
]);
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = __decorate([
        core_1.NgModule({
            imports: [
                Routing,
                shared_1.SharedModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                animations_1.NoopAnimationsModule
            ],
            declarations: [
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent
            ],
            providers: [services_1.AuthService, services_1.NoAuthGuard]
        })
    ], AuthModule);
    return AuthModule;
}());
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map