"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var services_1 = require("../shared/services");
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(seo, auth, router) {
        this.seo = seo;
        this.auth = auth;
        this.router = router;
        if (this.auth.isLoggedIn())
            this.router.navigate(['/admin']);
    }
    RegisterComponent.prototype.ngOnInit = function () {
        /*SEO META UPDATE*/
        this.seo.updateKeyword('RLD Webshop, Register, Authentication fro owner only');
        this.seo.updateTitle('RLD Webshop - Register for owner');
        this.seo.updateDescription('RLD Webshop Register Page');
        this.seo.updateSummary('RLD Webshop Register Page');
        this.seo.updateImage('/assets/images/headers/index2.jpg');
        this.createForm();
    };
    RegisterComponent.prototype.createForm = function () {
        this.form = new forms_1.FormGroup({
            name: new forms_1.FormControl(),
            email: new forms_1.FormControl(),
            password: new forms_1.FormControl(),
            password_confirmation: new forms_1.FormControl(),
        });
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        this.auth.register(this.form.value).then(function (response) { return null; }, function (error) { return _this.errors = JSON.parse(error._body); });
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'app-register',
            templateUrl: './register.component.html',
        }),
        __metadata("design:paramtypes", [services_1.SeoService,
            services_1.AuthService,
            router_1.Router])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map