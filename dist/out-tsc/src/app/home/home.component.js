"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var _1 = require("ngx-bootstrap/modal/");
var shared_1 = require("../shared");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(modalService, seo, cms, storage, homeservice) {
        this.modalService = modalService;
        this.seo = seo;
        this.cms = cms;
        this.storage = storage;
        this.homeservice = homeservice;
        this.image = '/assets/images/headers/index2.jpg';
        this.image2 = '/assets/images/featured2.png';
        this.loading = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.works = this.storage.get('works');
        this.service = this.storage.get('service');
        this.works = this.storage.get('works');
        //this.home       = this.storage.get('home');
        var homestatus = this.storage.status('home');
        if (homestatus) {
            this.home = this.storage.get('home');
        }
        else {
            this.cms.getPageCms('home').then(function (response) {
                _this.home = response.home;
            });
        }
        this.image = this.home[0].header_image == null ? this.image : this.home[0].header_image;
        this.image2 = this.home[0].package_image == null ? this.image2 : this.home[0].package_image;
        this.seo.updateTitle('RLD Webshop - Home');
        this.seo.updateAuthor('Ryan L. Dingle');
        this.seo.updateSummary('Welcome to my portfolio website');
        this.seo.updateDescription('RLD Webshop is a portfolio website owned by Ryan Dingle from Makati City, Philippines');
        this.seo.updateKeyword('Ryan Dingle, RLD Webshop, ryandingle09, ryandingle');
        this.seo.updateImage(this.image);
        this.createForm();
    };
    HomeComponent.prototype.createForm = function () {
        this.form = new forms_1.FormGroup({
            email: new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.email])
        });
    };
    HomeComponent.prototype.save = function (template) {
        var _this = this;
        this.loading = true;
        var data = new FormData();
        data.append('email', this.form.value.email);
        this.homeservice.store(data).then(function (response) {
            if (response.message == 'success') {
                _this.modalRef = _this.modalService.show(template);
                _this.loading = false;
                _this.errors = new shared_1.Errors;
            }
        }, function (error) {
            _this.errors = JSON.parse(error._body);
            _this.loading = false;
        });
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'app-home',
            templateUrl: './home.component.html',
        }),
        __metadata("design:paramtypes", [_1.BsModalService,
            shared_1.SeoService,
            shared_1.CmsService,
            shared_1.StorageService,
            shared_1.HomeService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map