"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultConfig = {
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down'
};
exports.SelectMultiConfig = Object.assign({}, exports.DefaultConfig, {
    labelField: 'label',
    valueField: 'value',
    maxItems: 1000
});
exports.ExampleValues = [
    {
        label: 'Angular',
        value: 'angular',
    }, {
        label: 'ReactJS',
        value: 'reactjs',
    }, {
        label: 'Ember JS',
        value: 'emberjs',
    }, {
        label: 'Ruby on Rails',
        value: 'ruby_on_rails',
    }
];
//# sourceMappingURL=selectize.config.js.map