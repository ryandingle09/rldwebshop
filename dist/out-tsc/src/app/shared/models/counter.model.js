"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CounterModel = /** @class */ (function () {
    function CounterModel() {
        this.portfolios = 0;
        this.blogs = 0;
        this.articles = 0;
        this.subscribers = 0;
        this.visitors = 0;
    }
    return CounterModel;
}());
exports.CounterModel = CounterModel;
//# sourceMappingURL=counter.model.js.map