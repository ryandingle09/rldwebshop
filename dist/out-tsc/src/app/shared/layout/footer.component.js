"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var models_1 = require("../../shared/models");
var services_1 = require("../../shared/services");
var FooterComponent = /** @class */ (function () {
    function FooterComponent(router, auth, storage) {
        this.router = router;
        this.auth = auth;
        this.storage = storage;
        this.login = false;
        this.page = 'home';
        this.status = this.auth.isLoggedIn();
        this.users = models_1.UserModel;
        this.today = Date.now();
    }
    FooterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sitedata = this.storage.get('sitedata');
        this.socialdata = this.storage.get('socialdata');
        this.router.events.subscribe(function (res) {
            _this.status = _this.auth.isLoggedIn();
            _this.users = _this.auth.getUserToken();
            if (res instanceof router_1.RoutesRecognized) {
                var route = res.state.root.firstChild;
                _this.page = route.data.page;
            }
            _this.login = (_this.page == 'register' || _this.page == 'login' || _this.page == 'home' || _this.page == 'about' || _this.page == 'works' || _this.page == 'contact' || _this.page == 'blog' || _this.page == 'blogdetail') ? false : true;
        });
    };
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'app-footer',
            templateUrl: './footer.component.html',
        }),
        __metadata("design:paramtypes", [router_1.Router,
            services_1.AuthService,
            services_1.StorageService])
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;
//# sourceMappingURL=footer.component.js.map