"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("./api.service");
var SiteService = /** @class */ (function () {
    function SiteService(api) {
        this.api = api;
    }
    SiteService.prototype.getSite = function () {
        return this.api.get('/site');
    };
    SiteService.prototype.getSocial = function () {
        return this.api.get('/social');
    };
    SiteService.prototype.update = function (data, id) {
        return this.api.update('/site/' + id + '/update', data);
    };
    SiteService.prototype.store = function (data) {
        return this.api.post('/site/post', data);
    };
    SiteService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], SiteService);
    return SiteService;
}());
exports.SiteService = SiteService;
//# sourceMappingURL=site.service.js.map