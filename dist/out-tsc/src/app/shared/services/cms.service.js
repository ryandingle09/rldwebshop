"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("./api.service");
var CmsService = /** @class */ (function () {
    function CmsService(api) {
        this.api = api;
    }
    CmsService.prototype.listByPost = function (data, page) {
        return this.api.listByPost('/cms/' + page + '/list', data);
    };
    CmsService.prototype.getPageCms = function (page) {
        return this.api.get('/cms/' + page + '/pagecms');
    };
    CmsService.prototype.get = function (id) {
        return this.api.get('/cms/' + id + '/get');
    };
    CmsService.prototype.edit = function (id) {
        return this.api.get('/cms/' + id + '/edit');
    };
    CmsService.prototype.store = function (data) {
        return this.api.post('/cms/post', data);
    };
    CmsService.prototype.update = function (data, id) {
        return this.api.post('/cms/' + id + '/update', data);
    };
    CmsService.prototype.delete = function (id) {
        return this.api.delete('/cms/' + id + '/delete');
    };
    CmsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], CmsService);
    return CmsService;
}());
exports.CmsService = CmsService;
//# sourceMappingURL=cms.service.js.map