"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("./api.service");
var CategoryService = /** @class */ (function () {
    function CategoryService(api) {
        this.api = api;
    }
    CategoryService.prototype.list = function () {
        return this.api.list('/category');
    };
    CategoryService.prototype.list2 = function (data) {
        return this.api.listByPost('/category/list', data);
    };
    CategoryService.prototype.store = function (data) {
        return this.api.post('/category/post', data);
    };
    CategoryService.prototype.edit = function (id) {
        return this.api.get('/category/' + id + '/edit');
    };
    CategoryService.prototype.update = function (data, id) {
        return this.api.update('/category/' + id + '/update', data);
    };
    CategoryService.prototype.delete = function (id) {
        return this.api.delete('/category/' + id + '/delete');
    };
    CategoryService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], CategoryService);
    return CategoryService;
}());
exports.CategoryService = CategoryService;
//# sourceMappingURL=category.service.js.map