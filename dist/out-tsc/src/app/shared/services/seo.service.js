"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var SeoService = /** @class */ (function () {
    function SeoService(meta, title) {
        this.meta = meta;
        this.title = title;
        this.lang = 'en_US';
        this.author = 'Ryan Dingle';
        this.appId = '234234';
        this.adminId = '234234';
        this.appType = 'website';
        this.siteName = 'RLD Webshop - Full Stack Web Developer';
    }
    SeoService.prototype.add = function (title, keywords, description, summary, url, image, website) {
        if (website === void 0) { website = this.appType; }
        /*search engine*/
        this.meta.addTag({ name: 'author', content: this.author });
        this.meta.addTag({ name: 'keywords', content: keywords });
        this.meta.addTag({ name: 'description', content: description });
        this.meta.addTag({ name: 'image', content: image });
        /*open graph for fb, etc...*/
        this.meta.addTag({ name: 'og:title', content: title });
        this.meta.addTag({ name: 'og:description', content: description });
        this.meta.addTag({ name: 'og:image', content: image });
        this.meta.addTag({ name: 'og:url', content: url });
        this.meta.addTag({ name: 'og:site_name', content: this.siteName });
        this.meta.addTag({ name: 'og:locale', content: this.lang });
        this.meta.addTag({ name: 'og:admins', content: this.adminId });
        this.meta.addTag({ name: 'og:app_id', content: this.appId });
        this.meta.addTag({ name: 'og:type', content: website });
        /*twitter*/
        this.meta.addTag({ name: 'twitter:card', content: summary });
        this.meta.addTag({ name: 'twitter:title', content: title });
        this.meta.addTag({ name: 'twitter:description', content: description });
        this.meta.addTag({ name: 'twitter:site', content: this.siteName });
        this.meta.addTag({ name: 'twitter:creator', content: this.author });
        this.meta.addTag({ name: 'twitter:image:src', content: image });
    };
    SeoService.prototype.update = function (data) {
        this.meta.updateTag(data);
    };
    SeoService.prototype.setTitle = function (title) {
        this.title.setTitle(title);
    };
    SeoService.prototype.updateSite = function (site) {
        this.meta.addTag({ name: 'og:site_name', content: site });
        this.meta.addTag({ name: 'twitter:site', content: site });
    };
    SeoService.prototype.updateTitle = function (title) {
        //this.meta.updateTag({ 'itemprop': 'name', content: title });
        this.meta.updateTag({ name: 'og:title', content: title });
        this.meta.updateTag({ name: 'twitter:title', content: title });
    };
    SeoService.prototype.updateAuthor = function (creator) {
        this.meta.updateTag({ name: 'twitter:creator', content: creator });
        this.meta.updateTag({ name: 'author', content: creator });
    };
    SeoService.prototype.updateKeyword = function (keyword) {
        this.meta.updateTag({ name: 'keywords', content: keyword });
    };
    SeoService.prototype.updateDescription = function (description) {
        this.meta.updateTag({ name: 'twitter:card', content: description });
        this.meta.updateTag({ name: 'description', content: description });
        //this.meta.updateTag({ 'itemprop': 'description', content: description });
        this.meta.updateTag({ name: 'og:description', content: description });
        this.meta.updateTag({ name: 'twitter:description', content: description });
    };
    SeoService.prototype.updateImage = function (image) {
        //this.meta.updateTag({ 'itemprop': 'image', content: image });
        this.meta.updateTag({ name: 'image', content: image });
        this.meta.updateTag({ name: 'og:image', content: image });
        this.meta.updateTag({ name: 'twitter:image:src', content: image });
    };
    SeoService.prototype.updateSummary = function (summary) {
        this.meta.updateTag({ name: 'twitter:card', content: summary });
    };
    SeoService.prototype.get = function (property) {
        this.meta.getTag(property);
    };
    SeoService.prototype.remove = function (property) {
        this.meta.removeTag(property);
    };
    SeoService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [platform_browser_1.Meta, platform_browser_1.Title])
    ], SeoService);
    return SeoService;
}());
exports.SeoService = SeoService;
//# sourceMappingURL=seo.service.js.map