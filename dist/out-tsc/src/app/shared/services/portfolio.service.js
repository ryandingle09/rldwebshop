"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("./api.service");
var PortfolioService = /** @class */ (function () {
    function PortfolioService(api) {
        this.api = api;
    }
    PortfolioService.prototype.list = function (data) {
        return this.api.listByPost('/project', data);
    };
    PortfolioService.prototype.get = function (id) {
        return this.api.get('/project' + id + '/get');
    };
    PortfolioService.prototype.getRecentPost = function () {
        return this.api.get('/project/recent-post');
    };
    PortfolioService.prototype.edit = function (id) {
        return this.api.get('/project/' + id + '/edit');
    };
    PortfolioService.prototype.delete = function (id) {
        return this.api.delete('/project/' + id + '/delete');
    };
    PortfolioService.prototype.store = function (data) {
        return this.api.post('/project/post', data);
    };
    PortfolioService.prototype.update = function (data, id) {
        return this.api.update('/project/' + id + '/update', data);
    };
    PortfolioService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], PortfolioService);
    return PortfolioService;
}());
exports.PortfolioService = PortfolioService;
//# sourceMappingURL=portfolio.service.js.map