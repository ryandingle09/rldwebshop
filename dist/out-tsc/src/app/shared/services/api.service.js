"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("../rxjs-operator");
var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        //public url  	= 'https://api.rldwebshop.xyz/api/v1';
        //private url 	= 'http://portfolioapi:90/api/v1';
        this.url = 'http://portfolioapi.test/api/v1';
        this.headers = new http_1.Headers({ 'Accept': 'application/json' });
    }
    ApiService.prototype.listByPost = function (params, data) {
        if (params === void 0) { params = null; }
        return this.http.post(this.url + params, data, { headers: this.headers })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ApiService.prototype.list = function (params) {
        if (params === void 0) { params = null; }
        return this.http.get(this.url + params, { headers: this.headers })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ApiService.prototype.get = function (params) {
        if (params === void 0) { params = null; }
        return this.http.get(this.url + params, { headers: this.headers })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ApiService.prototype.post = function (params, data) {
        if (params === void 0) { params = null; }
        return this.http.post(this.url + params, data, { headers: this.headers })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ApiService.prototype.update = function (params, data) {
        if (params === void 0) { params = null; }
        return this.http.post(this.url + params, data, { headers: this.headers })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ApiService.prototype.delete = function (params) {
        if (params === void 0) { params = null; }
        return this.http.delete(this.url + params, { headers: this.headers })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ApiService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    ApiService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;
//# sourceMappingURL=api.service.js.map