"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("./api.service");
var BlogService = /** @class */ (function () {
    function BlogService(api) {
        this.api = api;
    }
    BlogService.prototype.list = function (data) {
        return this.api.listByPost('/blog', data);
    };
    BlogService.prototype.getRecentPost = function () {
        return this.api.list('/blog/recent-post');
    };
    BlogService.prototype.listByCategory = function (data, id) {
        return this.api.listByPost('/blog/' + id + '/post-by-category', data);
    };
    BlogService.prototype.getCategoryList = function () {
        return this.api.list('/blog/category/list');
    };
    BlogService.prototype.get = function (id) {
        return this.api.get('/blog/' + id + '/get');
    };
    BlogService.prototype.getBySlug = function (slug) {
        return this.api.get('/blog/' + slug + '/show');
    };
    BlogService.prototype.edit = function (id) {
        return this.api.get('/blog/' + id + '/edit');
    };
    BlogService.prototype.delete = function (id) {
        return this.api.delete('/blog/' + id + '/delete');
    };
    BlogService.prototype.store = function (data) {
        return this.api.post('/blog/post', data);
    };
    BlogService.prototype.update = function (data, id) {
        return this.api.post('/blog/' + id + '/update', data);
    };
    BlogService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], BlogService);
    return BlogService;
}());
exports.BlogService = BlogService;
//# sourceMappingURL=blog.service.js.map