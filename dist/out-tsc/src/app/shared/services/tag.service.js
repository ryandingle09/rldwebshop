"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("./api.service");
var TagService = /** @class */ (function () {
    function TagService(api) {
        this.api = api;
    }
    TagService.prototype.list = function () {
        return this.api.list('/tag');
    };
    TagService.prototype.list2 = function (data) {
        return this.api.listByPost('/tag/list', data);
    };
    TagService.prototype.edit = function (id) {
        return this.api.get('/tag/' + id + '/edit');
    };
    TagService.prototype.delete = function (id) {
        return this.api.delete('/tag/' + id + '/delete');
    };
    TagService.prototype.store = function (data) {
        return this.api.post('/tag/post', data);
    };
    TagService.prototype.update = function (data, id) {
        return this.api.update('/tag/' + id + '/update', data);
    };
    TagService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], TagService);
    return TagService;
}());
exports.TagService = TagService;
//# sourceMappingURL=tag.service.js.map