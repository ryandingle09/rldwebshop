"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var _1 = require("ngx-bootstrap/modal/");
var shared_1 = require("../shared");
var ContactComponent = /** @class */ (function () {
    function ContactComponent(modalService, site, seo, storage, contactService) {
        this.modalService = modalService;
        this.site = site;
        this.seo = seo;
        this.storage = storage;
        this.contactService = contactService;
        this.image = '/assets/images/headers/index2.jpg';
        this.loading = false;
    }
    ContactComponent.prototype.ngOnInit = function () {
        this.contact = this.storage.get('contact');
        this.sitedata = this.storage.get('sitedata');
        this.image = this.contact[0].image == null ? this.image : this.contact[0].image;
        /*SEO META UPDATE*/
        this.seo.updateKeyword('RLD Webshop, Contact, Location, Time');
        this.seo.updateTitle('RLD Webshop - Contact');
        this.seo.updateDescription('RLD Webshop Contact Page');
        this.seo.updateSummary('RLD Webshop Contact Page');
        this.seo.updateAuthor('Ryan L. Dingle');
        this.seo.updateImage(this.image);
        this.createForm();
    };
    ContactComponent.prototype.createForm = function () {
        this.form = new forms_1.FormGroup({
            name: new forms_1.FormControl('', [forms_1.Validators.required]),
            email: new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.email]),
            message: new forms_1.FormControl('', [forms_1.Validators.required]),
        });
    };
    ContactComponent.prototype.save = function (template) {
        var _this = this;
        this.loading = true;
        var data = new FormData();
        data.append('name', this.form.value.name);
        data.append('email', this.form.value.email);
        data.append('message', this.form.value.message);
        this.contactService.store(data).then(function (response) {
            if (response.message == 'success') {
                _this.modalRef = _this.modalService.show(template);
                _this.loading = false;
                _this.errors = new shared_1.Errors;
            }
        }, function (error) {
            _this.errors = JSON.parse(error._body);
            _this.loading = false;
        });
    };
    ContactComponent = __decorate([
        core_1.Component({
            selector: 'app-contact"',
            templateUrl: './contact.component.html',
            styleUrls: ['./contact.component.css']
        }),
        __metadata("design:paramtypes", [_1.BsModalService,
            shared_1.SiteService,
            shared_1.SeoService,
            shared_1.StorageService,
            shared_1.ContactService])
    ], ContactComponent);
    return ContactComponent;
}());
exports.ContactComponent = ContactComponent;
//# sourceMappingURL=contact.component.js.map